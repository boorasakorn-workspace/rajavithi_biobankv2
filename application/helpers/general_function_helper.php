<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('calc_age') ){
	function calc_age($birthdate){
		$birthdate = strval($birthdate);
		$seperate = (strpos($birthdate, '_') ? '_' : (strpos($birthdate, '/') ? '/' : (strpos($birthdate, '-') ? '-' : ' ')));

		$birthdate = explode($seperate, $birthdate);
		$age = (date("md", date("U", mktime(0, 0, 0, $birthdate[0], $birthdate[1], $birthdate[2]))) > date("md")
			? ((date("Y") - $birthdate[2]) - 1)
			: (date("Y") - $birthdate[2])
		);
	    return $age;
	}
}

if ( !function_exists('convert_date_format')){
	function convert_date_format($fulldate){
		$timeString = strtotime($fulldate);
		$converted = date('d/m/Y', $timeString);
		return $converted;
	}	
}

if ( !function_exists('convert_ce_be') ){
	function convert_ce_be($fulldate, $seperate_chr = ''){
		$fulldate = strval($fulldate);
		$seperate = (strpos($fulldate, '_') ? '_' : (strpos($fulldate, '/') ? '/' : (strpos($fulldate, '-') ? '-' : ' ')));
		$seperate_chr = ($seperate_chr != '' ? $seperate_chr : $seperate);

		$ce_year=substr($fulldate,6,4)+543;
		$ce_month=substr($fulldate,3,2);
		$ce_date=substr($fulldate,0,2);
		$be_fulldate=$ce_date.$seperate_chr.$ce_month.$seperate_chr.$ce_year;
		return $be_fulldate;
	}
}

if ( !function_exists('convert_be_ce') ){
	function convert_be_ce($fulldate, $seperate_chr = ''){
		$fulldate = strval($fulldate);
		$seperate = (strpos($fulldate, '_') ? '_' : (strpos($fulldate, '/') ? '/' : (strpos($fulldate, '-') ? '-' : ' ')));
		$seperate_chr = ($seperate_chr != '' ? $seperate_chr : $seperate);

		$be_year=substr($fulldate,6,4)-543;
		$be_month=substr($fulldate,3,2);
		$be_date=substr($fulldate,0,2);
		$ce_fulldate=$be_date.$seperate_chr.$be_month.$seperate_chr.$be_year;
		return $ce_fulldate;
	}
}

if ( !function_exists('str_escape') ){
	function str_escape($str){
	    return "'".$str."'";
	}
}

if ( !function_exists('int_escape') ){
	function int_escape($str){
	    return intval($str);
	}
}