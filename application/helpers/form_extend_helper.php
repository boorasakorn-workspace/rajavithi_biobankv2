<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('form_config') ){
	function form_config(){
	    $CI =& get_instance();
	    $CI->load->config('formhelper');

        $config['form_class'] = $CI->config->item('form_class');
	    $config['input_class'] = $CI->config->item('input_class');
	    return $config;
	}
}

if( !function_exists('form_attribute') ){
    function form_attribute($name = ''){
        $config = form_config();
        $form_attribute = array(
            'id' => $name, 
            'name' => $name, 
            'class' => $config['form_class'],
        );
        return $form_attribute;
    }
}

if( !function_exists('input_attribute') ){
    function input_attribute($name = '', $value = '', $placeholder = ''){
        $config = form_config();
        $input_attribute = array(
            'id' => $name, 
            'name' => $name, 
            'class' => $config['input_class'],  
            'placeholder' => $placeholder,
            'value' => $value,
        );
        return $input_attribute;
    }
}

if( !function_exists('datepicker_attribute') ){
    function datepicker_attribute($name = '', $value = '', $language = 'th-th', $placeholder = ''){
        $config = form_config();
        $input_attribute = array(
            'id' => $name, 
            'name' => $name, 
            'class' => $config['input_class'],  
            'placeholder' => $placeholder,
            'value' => $value,
            'data-provide' => 'datepicker',
            'data-date-language' => $language,
        );
        return $input_attribute;
    }
}

if( !function_exists('dropdown_attribute') ){
    function dropdown_attribute($id =''){
        $config = form_config();
        $dropdown_attribute = array(
            'id' => $id,   
            'class' => $config['input_class'],
        );
        return $dropdown_attribute;
    }
}

if( !function_exists('form_extend') ){
    function form_extend($url, $name = ''){
        return form_open($url, form_attribute($name));
    }
}

if( !function_exists('form_multipart_extend') ){
    function form_multipart_extend($url, $name = ''){
        return form_open_multipart($url, form_attribute($name));
    }
}

if( !function_exists('input_text') ){
    function input_text($name = '', $value = '', $placeholder = ''){
        return form_input(input_attribute($name, $value, $placeholder));
    }
}

if( !function_exists('input_password') ){
    function input_password($name = '', $value = '', $placeholder = ''){
        return form_password(input_attribute($name, $value, $placeholder));
    }
}

if( !function_exists('dropdown_extend') ){
    function dropdown_extend($name = '', $options, $selected = ''){
        return form_dropdown($name, $options, $selected, dropdown_attribute($name));
    }
}

if( !function_exists('dropdown_prefix') ){
    function dropdown_prefix($selected = ''){
        $prefix_options = array(
            'Mr.'   =>  'Mr.',
            'Mrs.'  =>  'Mrs.',
            'Miss.' =>  'Miss.',
        );
        return dropdown_extend('prefix', $prefix_options, $selected);
    }
}

if( !function_exists('dropdown_gender') ){
    function dropdown_gender($selected = ''){
        $prefix_options = array(
            'ชาย'   =>  'ชาย',
            'หญิง'  =>  'หญิง',
        );
        return dropdown_extend('gender', $prefix_options, $selected);
    }
}

if( !function_exists('dropdown_usertype') ){
    function dropdown_usertype($selected = ''){
        $prefix_options = array(
            '3'  =>  'User',
            '1'   =>  'Staff',
            '2'  =>  'Head Staff',
        );
        return dropdown_extend('usertype', $prefix_options, $selected);
    }
}

if( !function_exists('datepicker') ){
    function datepicker($name = '', $value = '', $language = 'en', $placeholder = ''){
        return form_input(datepicker_attribute($name, $value, $language, $placeholder));        
    }
}

if( !function_exists('datepicker_th') ){
    function datepicker_th($name = '', $value = '', $language = 'th', $placeholder = ''){
        return form_input(datepicker_attribute($name, $value, $language, $placeholder));        
    }
}

if( !function_exists('form_group_input') ){
    function form_group_input($name = '', $value = '', $labeltext = '', $placeholder = ''){
        $labeltext = ($labeltext != '' ? $labeltext : ucfirst($name));
        $formgroup = '<div class="form-group">'."\r\n";
        $formgroup .= form_label($labeltext, $name);
        $formgroup .= input_text($name, $value, $placeholder);
        $formgroup .= '</div>'."\r\n";
        return $formgroup;
    }
}

if( !function_exists('form_group_password') ){
    function form_group_password($name = '', $value = '', $labeltext = '', $placeholder = ''){
        $labeltext = ($labeltext != '' ? $labeltext : ucfirst($name));
        $formgroup = '<div class="form-group">'."\r\n";
        $formgroup .= form_label($labeltext, $name);
        $formgroup .= input_password($name, $value, $placeholder);
        $formgroup .= '</div>'."\r\n";
        return $formgroup;
    }
}