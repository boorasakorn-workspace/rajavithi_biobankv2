<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessionextend {

    function SetSite($Site){
        $CI =& get_instance();
        $CI->session->set_userdata('site',$Site);
    }

    function GetSite(){
        $CI =& get_instance();
        $result = $CI->session->userdata('site');
        return $result;
    }

    function SetFreezer($ID){
        $CI =& get_instance();
        $CI->session->set_userdata('FreezerID',$ID);
    }

    function GetFreezer(){
        $CI =& get_instance();
        $result = $CI->session->userdata('FreezerID');
        return $result;
    }

    function SetID($id){
		$CI =& get_instance();
        $CI->session->set_userdata('id',$id);
    }

    function SetSession($data){
        $CI =& get_instance();
        $CI->session->set_userdata($data);
    }

    function HasID(){
		$CI =& get_instance();
	    $checkresult = $CI->session->has_userdata('id');
    	return $checkresult;
    }

    function currentID(){
		$CI =& get_instance();
	    $result = $CI->session->userdata('id');
    	return $result;
    }

    function userType(){
        $CI =& get_instance();
        $result = $CI->session->userdata('type');
        return $result;
    }

}

?>