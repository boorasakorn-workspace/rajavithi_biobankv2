<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_Module extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
    }

    public function Template($TemplateView,$TemplateData){
        $this->load->module($TemplateData['Module']);
        $this->load->view($TemplateView,$TemplateData);        
    }
}
