<!DOCTYPE html>
<html>
	<head>
		<title><?=(isset($Site_Title) ? $Site_Title : (defined(SITE_TITLE) ? SITE_TITLE : "DashQueue"));?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="content-type" content="text/javascript; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php 
            $template_css = array(
                'Preloader' => 'vendors/Preloader-master/preloader.css',
                'fontswitcher' => 'css/fontswitcher/fontswitcher.css', 
                'bootstrap4.3.1' => 'vendors/bootstrap/css/bootstrap.min.css', 
                'fontawesome5.11.2' => 'vendors/fontawesome/css/all.min.css', 
                'sweetalert' => 'vendors/sweetalert/sweetalert.css',
                'responsivescale' => 'css/ResponsiveScale.css', 
                'main' => 'css/ManagementStyle.css', 
            );
            echo assets_css($template_css);
        ?>
		<?=(isset($css) ? assets_css($css) : '');?>
		<?=single_js('js/viewport.js');?>		
	</head>
	<body>
		<div class="management-container">
			<div id="management_header" class="header" style="height:75px;border-bottom:1px solid #2C5364;">
				<div style="width:20rem;height: 100%;padding:0.5% 15px;border-right:1px solid #C8C8C8;">
					<?=assets_img('images/icon_header.png','style="height:60px;margin: 0 auto;"');?>
				</div>
				<div class="col text-right">
					<ul class="inline seperate_box" style="font-size: 1.5rem;font-weight: 200;">
						<li id="li_login" class="dropdown">
				            <button class="dropdown-toggle none" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				            	<span id="span_Username" class="sub"><?=$this->session->userdata('user_username');?></span> 
				            	<span id="span_Fullname" ><?=$this->session->userdata('user_name');?></span>
				            </button>
							<div class="dropdown-menu" style="font-size:1.5rem;" aria-labelledby="dropdownMenuButton">
								<a id="acc_logout" class="dropdown-item" href="<?=base_url('Management/Logout');?>">Logout</a>
							</div>

			            </li>
					</ul>
				</div>
			</div>

			<div id="management_content" class="content" style="position: relative;height:calc(100% - 75px);overflow-y: auto;">
				<!-- Management Content -->
					<?php
						if( isset($Content) && count($Content)>0 ){
							foreach($Content as $result) : 
								$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
							endforeach;				
						}
					?>
				<!-- /Management Content -->

			</div>

			<div id="management_sidepanel" class="sidenav_panel active" style="height:calc(100% - 75px);">
				<a class="panel_btn" data-sidenav="#management_sidepanel"><i class="fa fa-align-justify"></i></a>
				<div class="side_content">
					<ul class="list-unstyled sidelist">
						<?php /*
			            <li id="Dashboard">
			                <a href="<?=base_url('Management/Dashboard');?>"><i class="fas fa-desktop"></i>Dashboard</a> 
			            </li> */ ?>
			            <?php if($this->sessionextend->userType() != 3): ?>
			                <li id='Input'>
			                    <a href="<?=base_url('Management/Input');?>"><i class="fas fa-th-list"></i>Input</a> 
			                </li>
			            	<li id='Sample'>
			            		<a href="<?=base_url('Management/Sample');?>"><i class='fas fa-barcode'></i>Database</a>
			            	</li>
			            <?php endif; ?>
			            <li id="Search">
		                    <a href="<?=base_url('Management/Search');?>"><i class="fas fa-search"></i>Search</a> 
		                </li>
		                <?php /*
		                <li id="Report">
		                    <a href="<?=base_url('Management/Report');?>"><i class="fa fa-folder-open"></i>Report</a> 
		                </li> */ ?>
			            <?php if($this->sessionextend->userType() != 3 && $this->sessionextend->userType() != 1): ?>
			            	<li class='has-sub'>
		                        <a data-action="sublist" href="#"><i class='fa fa-wrench'></i>Create</a>    
		                        <ul class='list-unstyled sidelist'>
		                            <li id='Box'>
		                                <a href="<?=base_url('Management/Master/Box');?>">Box</a> 
		                            </li>
		                            <li id='Freezer'>
		                                <a href="<?=base_url('Management/Master/Freezer');?>">Freezer</a> 
		                            </li>
		                            <?php /*
		                            <li id='Report_Table'>
		                                <a href="<?=base_url('Management/Master/Report');?>">Report Table</a> 
		                            </li> */ ?>
		                        </ul>
		                    </li>
			            <?php endif; ?>
		                <li class="has-sub">
		                    <a data-action="sublist" href="#"><i class="fa fa-cog"></i>Manage</a>    
		                    <ul class="list-unstyled sidelist">
		                        <li id="Account">
		                            <a href="<?=base_url('Account');?>">Account</a>
		                        </li>
		                        <?php if($this->sessionextend->userType() != 3 && $this->sessionextend->userType() != 1): ?>
		                        	<li id='User'>
		                                <a href="<?=base_url('Account/User');?>">User</a>
		                            </li>
		                        <?php endif; ?>
		                    </ul>
		                </li>
					</ul>
				</div>
			</div>
		</div>

		<?php 
            $template_js = array(
                'popper' => 'vendors/popper/popper.min.js', 
                'jquery3.4.1' => 'vendors/jquery/jquery-3.4.1.min.js', 
                'bootstrap4.3.1' => 'vendors/bootstrap/js/bootstrap.min.js',
                'jquery-resizer' => 'vendors/jquery.resize-master/jquery.resize.js',
                'moment' => 'vendors/moment/moment-with-locales.js',
                'Preloader' => 'vendors/Preloader-master/preloader.js',
                'sweetalert' => 'vendors/sweetalert/sweetalert.min.js',
            );
            echo assets_js($template_js);
        ?>
		<?=(isset($js) ? assets_js($js) : '');?>
		<?=(isset($node_modules) ? assets_node($node_modules) : '');?>

		<script type="text/javascript">
			function postAJAX(urlpath,data,callback,callback_err){
				$.ajax({
			        url: urlpath,
			        method: 'POST',
			        dataType: 'json',
			        data: data,
			        headers: {
			            'Access-Control-Allow-Origin': '*',
			            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
			            'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept',
			        }, 
			        success: function(result){
			            console.log('AJAX Result : ' , result);
			            if (callback) {
						    callback(result);
						}
			        },
			        error: function(err){
			        	console.log('AJAX Error : ',err);
			            if (callback_err) {
						    callback_err(err);
						}else{
							swal({
			                    title: "Failed",
			                    type: "error",
			                    confirmButtonClass: "btn-danger",
			                    confirmButtonText: "Close",
			                });
						}
			        },
			    });
			}

			$(document).ready(function(){

				if ($('[data-date-language="th"]').length) {
				    $('[data-date-language="th"]').datepicker({language:'th',format:'dd/mm/yyyy'});
				}

				if ($('[data-provide="datepicker"]').length) {
				    $('i.icon-arrow-left').addClass('fas');
				    $('.table-condensed .next').text('<i class="fas fa-arrow-right"></i>');
				}
		        $('input').keyup(function(event){
		            event.preventDefault();
		        });
				/* Sidenav */
					$(document).on("click", '[data-sidenav]', function () {
						var Sidenav = $(this).data('sidenav');
						$(this).toggleClass('active');
						$(Sidenav).toggleClass('active');

			        	var PosLeft = parseInt($(Sidenav).css('left')) * ( $(Sidenav).hasClass('active') ? (-1) : 0 );
			        	$('#management_content').css('left', PosLeft);
			        	
			        	var ContentWidth =  parseInt($('#management_content').css('width'));
			        	var SidenavWidth =  parseInt($(Sidenav).css('width'));
			        	var NewWidth = $(Sidenav).hasClass('active') ? ContentWidth-SidenavWidth : ContentWidth+SidenavWidth;

			        	$('#management_content').css('width', NewWidth);
					});

					$('.sidenav_panel.active').each(function(i, obj) {
					    var SidenavID = $(this).attr('id');
					    var SidenavWidth = parseInt($(this).css('width'));
					    var ContentWidth = parseInt($('#management_content').css('width'));

					    ContentWidth -= SidenavWidth;
					    if( !$('[data-sidenav="#'+SidenavID+'"]').hasClass('active') ){ $('[data-sidenav="#'+SidenavID+'"]').addClass('active'); }
					    $('#management_content').css('width', ContentWidth);
					    $('#management_content').css('left', SidenavWidth);
					});

					$(document).on("click", '[data-action="sublist"]', function () {
						$(this).parent('li').toggleClass('show');
					});
				/* Sidenav */

				/* FormSubmit Ajax */
					$(document).on('click','[form_submit]',function(){
						var action_path = $(this).closest('form').attr('action');
						var formData = $(this).closest('form').serialize();
						postAJAX(action_path,formData,function(data){
							console.log(data);
							if(data['function']){
								switch(data['function']){
									case 'refresh':
										location.reload();
										break;
									case 'redirect':
										window.location.href = '<?=base_url();?>' + data['url'];
										break;
									case 'alert':
									console.log("TEST");
										swal({
						                    title: data['modal-data']['title'],
						                    text: data['modal-data']['text'],
						                    type: data['modal-data']['type'],
						                });
						                break;
								}
							}
						});

					});
				/* FormSubmit Ajax */

			});
		</script>
		
		<?php 
			if( isset($Script) && count($Script)>0 ){
				foreach($Script as $result) : 
					$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
				endforeach;
			}
		?>

	</body>
</html>
