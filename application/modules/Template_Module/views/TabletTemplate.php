<!DOCTYPE html>
<html>
	<head>
		<title><?=(isset($Site_Title) ? $Site_Title : (defined(SITE_TITLE) ? SITE_TITLE : "DashQueue"));?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="content-type" content="text/javascript; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php 
            $template_css = array(
                'Preloader' => 'vendors/Preloader-master/preloader.css',
                'fontswitcher' => 'css/fontswitcher/fontswitcher.css', 
                'bootstrap4.3.1' => 'vendors/bootstrap/css/bootstrap.min.css', 
                'fontawesome5.11.2' => 'vendors/fontawesome/css/all.min.css', 
                'sweetalert' => 'vendors/sweetalert/sweetalert.css',
                'responsivescale' => 'css/ResponsiveScale.css', 
                'main' => 'css/ManagementStyle.css', 
            );
            echo assets_css($template_css);
        ?>
		<?=(isset($css) ? assets_css($css) : '');?>
		<?=single_js('js/viewport.js');?>		
	</head>
	<body>
		<div class="management-container">
			<div id="management_content" class="content h-100" style="position: relative;overflow-y: auto;">
				<!-- Management Content -->
					<?php
						if( isset($Content) && count($Content)>0 ){
							foreach($Content as $result) : 
								$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
							endforeach;				
						}
					?>
				<!-- /Management Content -->

				<div class="container-fluid">
					<div class="row">
						<div class="col text-center">
							<a href="<?=base_url('Kiosk/Menu');?>">
		                        <button class="btn btn-primary m-b-40 m-t-10" style="font-size:2rem;">
		                            Menu
		                        </button>
		                    </a>
							<a href="<?=base_url('Kiosk/Logout');?>">
		                        <button class="btn btn-danger m-b-40 m-t-10" style="font-size:2rem;">
		                            Logout
		                        </button>
		                    </a>
						</div>
					</div>
				</div>
			</div>


		</div>

		<?php 
            $template_js = array(
                'popper' => 'vendors/popper/popper.min.js', 
                'jquery3.4.1' => 'vendors/jquery/jquery-3.4.1.min.js', 
                'bootstrap4.3.1' => 'vendors/bootstrap/js/bootstrap.min.js',
                'jquery-resizer' => 'vendors/jquery.resize-master/jquery.resize.js',
                'moment' => 'vendors/moment/moment-with-locales.js',
                'Preloader' => 'vendors/Preloader-master/preloader.js',
                'sweetalert' => 'vendors/sweetalert/sweetalert.min.js',
            );
            echo assets_js($template_js);
        ?>
		<?=(isset($js) ? assets_js($js) : '');?>
		<?=(isset($node_modules) ? assets_node($node_modules) : '');?>

		<script type="text/javascript">
			function postAJAX(urlpath,data,callback,callback_err){
				$.ajax({
			        url: urlpath,
			        method: 'POST',
			        dataType: 'json',
			        data: data,
			        headers: {
			            'Access-Control-Allow-Origin': '*',
			            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
			            'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept',
			        }, 
			        success: function(result){
			            console.log('AJAX Result : ' , result);
			            if (callback) {
						    callback(result);
						}
			        },
			        error: function(err){
			        	console.log('AJAX Error : ',err);
			            if (callback_err) {
						    callback_err(err);
						}else{
							swal({
			                    title: "Failed",
			                    type: "error",
			                    confirmButtonClass: "btn-danger",  
			                    confirmButtonText: "Close",

			                });
						}
			        },
			    });
			}

			$(document).ready(function(){

				if ($('[data-date-language="th"]').length) {
				    $('[data-date-language="th"]').datepicker({language:'th',format:'dd/mm/yyyy'});
				}

				/* FormSubmit Ajax */
					$(document).on('click','[form_submit]',function(){
						var action_path = $(this).closest('form').attr('action');
						var formData = $(this).closest('form').serialize();
						postAJAX(action_path,formData,function(data){
							console.log(data);
							if(data['function']){
								switch(data['function']){
									case 'refresh':
										location.reload();
										break;
									case 'redirect':
										window.location.href = '<?=base_url();?>' + data['url'];
										break;
									case 'alert':									
										swal({
						                    title: data['modal-data']['title'],
						                    text: data['modal-data']['text'],
						                    type: data['modal-data']['type'],
						                });
						                break;
								}
							}
						});

					});
				/* FormSubmit Ajax */

			});
		</script>
		
		<?php 
			if( isset($Script) && count($Script)>0 ){
				foreach($Script as $result) : 
					$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
				endforeach;
			}
		?>

	</body>
</html>
