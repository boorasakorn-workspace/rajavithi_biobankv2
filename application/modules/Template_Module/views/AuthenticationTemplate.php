<!DOCTYPE html>
<html>
	<head>
		<title><?=(isset($Site_Title) ? $Site_Title : (defined(SITE_TITLE) ? SITE_TITLE : "DashQueue"));?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('static/images/logo_bio.ico');?>"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="content-type" content="text/javascript; charset=utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php 
            $template_css = array(
                'Preloader' => 'vendors/Preloader-master/preloader.css',
                'fontswitcher' => 'css/fontswitcher/fontswitcher.css', 
                'bootstrap4.3.1' => 'vendors/bootstrap/css/bootstrap.min.css', 
                'fontawesome5.11.2' => 'vendors/fontawesome/css/all.min.css', 
                'sweetalert' => 'vendors/sweetalert/sweetalert.css',
                'responsivescale' => 'css/ResponsiveScale.css', 
                'main' => 'css/ManagementStyle.css', 
            );
            echo assets_css($template_css);
        ?>
		<?=(isset($css) ? assets_css($css) : '');?>
		<?=single_js('js/viewport.js');?>		
	</head>
	<body>
		<div class="container py-5">
		    <div class="row">
		        <div class="col-md-12">
					<div class="col-md-12 text-center mb-5">
			            <img src="">
					</div>
		            <div class="row">
		                <div class="col-md-6 mx-auto">
		                    <!-- Content -->
								<?php
									if( isset($Content) && count($Content)>0 ){
										foreach($Content as $result) : 
											$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
										endforeach;				
									}
								?>
							<!-- Content -->
		                </div>
		            </div>
		        </div>
		    </div>
		</div>		

		<?php 
            $template_js = array(
                'popper' => 'vendors/popper/popper.min.js', 
                'jquery3.4.1' => 'vendors/jquery/jquery-3.4.1.min.js', 
                'bootstrap4.3.1' => 'vendors/bootstrap/js/bootstrap.min.js',
                'moment' => 'vendors/moment/moment-with-locales.js',
                'Preloader' => 'vendors/Preloader-master/preloader.js',
                'sweetalert' => 'vendors/sweetalert/sweetalert.min.js',
            );
            echo assets_js($template_js);
        ?>
		<?=(isset($js) ? assets_js($js) : '');?>
		<?=(isset($node_modules) ? assets_node($node_modules) : '');?>

		<script type="text/javascript">

			function postAJAX(urlpath,data,callback,callback_err){
				$.ajax({
			        url: urlpath,
			        method: 'POST',
			        dataType: 'json',
			        data: data,
			        headers: {
			            'Access-Control-Allow-Origin': '*',
			            'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
			            'Access-Control-Allow-Headers': 'Authorization, Origin, X-Requested-With, Content-Type, Accept',
			        }, 
			        success: function(result){
			            console.log('AJAX Result : ' , result);
			            if (callback) {
						    callback(result);
						}
			        },
			        error: function(err){
			        	console.log('AJAX Error : ',err);
			            if (callback_err) {
						    callback_err(err);
						}else{
							swal({
			                    title: "Failed",
			                    type: "error",
			                    confirmButtonClass: "btn-danger",
			                    confirmButtonText: "Close",
			                });
						}
			        },
			    });
			}

			$(document).ready(function(){

				if ($('[data-date-language="th"]').length) {
				    $('[data-date-language="th"]').datepicker({language:'th',format:'dd/mm/yyyy'});
				}

				$(document).on('click','[form_submit]',function(){
					var action_path = $(this).closest('form').attr('action');
					var formData = $(this).closest('form').serialize();
					postAJAX(action_path,formData,function(data){
						if(data['function']){
							switch(data['function']){
								case 'refresh':
									location.reload();
									break;
								case 'redirect':
									window.location.href = '<?=base_url();?>' + data['url'];
									break;
								case 'alert':									
									swal({
					                    title: data['modal-data']['title'],
					                    text: data['modal-data']['text'],
					                    type: data['modal-data']['type'],
					                });
					                if( $('#scan_idcard').length ){
					                	$('#scan_idcard').val('');
					                }
					                break;
							}
						}
					});

				});
			});

		</script>
		
		<?php 
			if( isset($Script) && count($Script)>0 ){
				foreach($Script as $result) : 
					$this->load->view((isset($Module) ? $Module.($Module != '' ? '/' : '') : '/').$result);
				endforeach;
			}
		?>

	</body>
</html>
