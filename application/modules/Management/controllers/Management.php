<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');

        if(!$this->sessionextend->HasID()){
            redirect('Authentication/Management','refresh');
        }

        if(NULL !== $this->sessionextend->GetSite() && $this->sessionextend->GetSite() != 'Management'){
            redirect($this->sessionextend->GetSite(),'refresh');
        }
        $this->load->module('Service');
    }

    public function Index(){
        redirect('Management/Search','refresh');
    }

    public function Logout(){
        foreach($_SESSION as $key => $val){
            if ($key !== 'site'){
              unset($_SESSION[$key]);
            }
        }
        redirect('','refresh');
    }

    public function Dashboard(){
        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Management',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Dashboard/Dashboard',
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template);    	
    }

    public function Sample($Year = NULL){
        $this->load->module('Template_Module');
        $Template = array(
            'Module' => 'Management',
            'Site_Title' => 'Database',
            'Content' => array(
                'Main' => 'Sample/Sample',
                'Modal' => 'Sample/Sample_Modal',
            ),
            'Script' => array(
                'MainScript' => 'Sample/Sample_Script',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.js',
            ),
            'Data' => array(
                'AnuSample' => $this->service->AnuData('Sample'),
                'SamplePrefix' => $this->service->AnuData('Prefix'),
                'SampleType' => $this->service->AnuData('SampleType'),
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template);
    }

    public function Input($SelectedInput = NULL,$ID = NULL){
        $this->load->module('Template_Module');
        switch ($SelectedInput) {
            case 'All':
                $InputTitle = 'Input';
                $InputData = array(
                    'FreezerData'   =>  $this->service->ListAdd('Freezer'),
                    'FreezerFloorData'  =>  $this->service->ListAdd('FreezerFloor'),
                    'RackData'  =>  $this->service->ListAdd('Rack'),
                    'RackFloorData' =>  $this->service->ListAdd('RackFloor'),
                    'FreezerBoxData' => $this->service->ListAdd('Box'),
                );
                $InputContent = array(
                    'Main' => 'Input/Input',
                    'Modal' => 'Input/Input_Modal',
                );
                $ScriptContent = array(
                    'MainScript' => 'Input/Input_Script',
                );
                break;
            case 'Freezer':
                $InputTitle = 'Freezer';
                $InputData = $this->service->ListAdd('Freezer');
                $InputContent = array(
                    'Main' => 'Input/Freezer/Freezer',
                );
                break;
            case 'FreezerFloor':
                $InputTitle = 'FreezerFloor';
                $InputData = $this->service->ListAdd('FreezerFloor',$ID);
                $InputContent = array(
                    'Main' => 'Input/FreezerFloor/FreezerFloor',
                );
                break;
            case 'Rack':
                $InputTitle = 'Rack';
                $InputData = $this->service->ListAdd('Rack',$ID);
                $InputContent = array(
                    'Main' => 'Input/Rack/Rack',
                );
                break;
            case 'RackFloor':
                $InputTitle = 'RackFloor';
                $InputData = $this->service->ListAdd('RackFloor',$ID);
                $InputContent = array(
                    'Main' => 'Input/RackFloor/RackFloor',
                );
                break;
            case 'Box':
                $InputTitle = 'Box';
                $InputData = $this->service->ListAdd('Box',$ID,'RackFloor');
                $InputContent = array(
                    'Main' => 'Input/Box/Box',
                );
                break;
            case 'BoxInput':
                $InputTitle = 'Box';
                $BoxData = $this->service->ListData('Box',$ID);
                $InputData = array(
                    'BoxID' => $BoxData->BOX_ID,
                    'BoxInput' => $this->service->DataAdd('Box',$ID),
                    'Slot' => array(
                        'Rows' => $BoxData->BOX_ROW,
                        'Columns' => $BoxData->BOX_COLUM,
                    ),
                );
                $InputContent = array(
                    'Main' => 'Input/BoxInput/BoxInput',
                    'Modal' => 'Input/BoxInput/BoxInput_Modal',
                );
                $ScriptContent = array(
                    'MainScript' => 'Input/BoxInput/BoxInput_Script',
                );

                //echo '<pre>'.var_export($BoxData,TRUE).'</pre>';
                //echo '<pre>'.var_export($InputData['BoxInput']->result(),TRUE).'</pre>';
                //die();
                break;    
            default:
                redirect('Management/Input/All','refresh');
                break;
        }

        $Template = array(
            'Module' => 'Management',
            'Site_Title' => (isset($InputTitle) && $InputTitle != '' ? $InputTitle : 'Input'),
            'Content' => (isset($InputContent) && count($InputContent) > 0 ? $InputContent : array()),
            'Script' => (isset($ScriptContent) && count($ScriptContent) > 0 ? $ScriptContent : array()),
            'Data' => array(
                'InputData' => $InputData,
            )
        );
        $this->template_module->Template('ManagementTemplate',$Template);
    }

    public function Search(){
        $this->load->module('Template_Module');
        $Template = array(
            'Module' => 'Management',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Search/Search',
                'Modal' => 'Search/Search_Modal',
                'SampleModal' => 'Sample/Sample_Modal',
            ),
            'Script' => array(
                'MainScript' => 'Search/Search_Script',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.js',
            ),
            'Data' => array(
                'Sample' => $this->service->AnuData('Sample'),
                'SamplePrefix' => $this->service->AnuData('Prefix'),
                'SampleType' => $this->service->AnuData('SampleType'),
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template); 
    }

    public function Report(){
        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Management',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Report/Report',
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template); 
    }

    public function Master($SelectedInput = NULL,$ID = NULL){
        $this->load->module('Template_Module');
        switch ($SelectedInput) {
            case 'Box':
                $InputTitle = 'Create Box';
                $InputData = array(
                    'Box' => $this->service->ListData('Box'),
                    'BoxLocate' => $this->service->DataLocate('Box'),
                );
                $InputContent = array(
                    'Main' => 'Master/Box/Box',
                    'Modal' => 'Master/Box/Box_Modal',
                );
                $ScriptContent = array(
                    'MainScript' => 'Master/Box/Box_Script',
                );
                break;
            case 'Freezer':
                $InputTitle = 'Create Freezer';
                $InputData = array(
                    'FreezerData'   =>  $this->service->ListAdd('Freezer'),
                    'FreezerFloorData'  =>  $this->service->ListAdd('FreezerFloor'),
                    'RackData'  =>  $this->service->ListAdd('Rack'),
                    'RackFloorData' =>  $this->service->ListAdd('RackFloor'),
                );
                $InputContent = array(
                    'Main' => 'Master/Freezer/Freezer',
                    'Modal' => 'Master/Freezer/Freezer_Modal'
                );
                $ScriptContent = array(
                    'MainScript' => 'Master/Freezer/Freezer_Script',
                );
                break;
            default:
                redirect('Management','refresh');
                break;
        }

        $Template = array(
            'Module' => 'Management',
            'Site_Title' => (isset($InputTitle) && $InputTitle != '' ? $InputTitle : 'Create'),
            'Content' => (isset($InputContent) && count($InputContent) > 0 ? $InputContent : array()),
            'Script' => (isset($ScriptContent) && count($ScriptContent) > 0 ? $ScriptContent : array()),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.js',
            ),
            'Data' => array(
                'InputData' => $InputData,
            )
        );
        $this->template_module->Template('ManagementTemplate',$Template); 
    }

}