<div class="card">
    <div class="card-header">
        <h3 class="title-2 m-b-40"><?=(isset($Site_Title) && $Site_Title != '' ? $Site_Title : 'Input');?></h3>
    </div>
    <div class="card-body">
    <!-- Freezer Body -->
        <div class="row">
            <div class="col-4" style="height:100%;overflow-y: auto;">
                <table id="boxadd_active" class="table table-striped table-bordered">
                    <tr>
                        <th style="white-space: nowrap; text-align: left;">#</th> 
                        <th style="white-space: nowrap; text-align: left;">Barcode</th>
                        <th style="white-space: nowrap; text-align: left;">Well</th>
                        <th style="white-space: nowrap; text-align: left;">Manage</th>
                    </tr>
                    <?php
                        $InputCount = 0;
                        if(isset($Data['InputData']['BoxInput']) && $Data['InputData']['BoxInput']->num_rows() > 0){
                            foreach ($Data['InputData']['BoxInput']->result() as $Row) : 
                                $InputCount ++;
                    ?>
                        <tr>
                            <td style="white-space: nowrap; text-align: left;"><?=$InputCount;?></td>
                            <td style="white-space: nowrap; text-align: left;"><?=$Row->SAMPLE_SID;?></td>
                            <td data-boxwell_active="<?=$Row->BOXADD_WELL;?>" style="white-space: nowrap; text-align: left;"><?=$Row->BOXADD_WELL;?></td>
                            <td style="white-space: nowrap; text-align: left;">
                                <button type="button" class="btn btn-sm btn-danger mb-1" data-boxid="<?=$Data['InputData']['BoxID'];?>" data-boxwell_remove="<?=$Row->BOXADD_WELL;?>">Remove</button>
                            </td>
                        </tr>
                    <?php 
                            endforeach;
                        }
                    ?>
                </table>                
            </div>
            <div class="col-8">
                <table id="boxwell_slot" class="table table-striped table-bordered text-center">
                    <tr>
                        <th style="border-top-color: #FFFFFF;border-left-color: #FFFFFF;background: #FFFFFF;"> </th>
                        <?php
                            if(isset($Data['InputData']['Slot']['Columns']) && $Data['InputData']['Slot']['Columns'] > 0){
                                for ($ColumnsCount=0; $ColumnsCount < $Data['InputData']['Slot']['Columns']; $ColumnsCount++) : 
                        ?>
                            <th><?=chr(65+$ColumnsCount);?></th>
                        <?php 
                                endfor;
                            } 
                        ?>  
                    </tr>
                    <?php
                        if(isset($Data['InputData']['Slot']['Rows']) && $Data['InputData']['Slot']['Rows'] > 0){
                            for ($RowsCount=1; $RowsCount <= $Data['InputData']['Slot']['Rows']; $RowsCount++) : 
                    ?>
                        <tr>
                            <th width="5%" style="text-align: center;"><?=$RowsCount;?></th>
                            <?php
                                if(isset($Data['InputData']['Slot']['Columns']) && $Data['InputData']['Slot']['Columns'] > 0){
                                    for ($ColumnsCount=0; $ColumnsCount < $Data['InputData']['Slot']['Columns']; $ColumnsCount++) : 
                            ?>
                                <td class="boxwell" data-boxwell="<?=chr(65+$ColumnsCount).$RowsCount;?>">
                                    <button class="none" data-boxid="<?=$Data['InputData']['BoxID'];?>" data-boxwell_slot="<?=chr(65+$ColumnsCount).$RowsCount;?>" style="width:100%;height:100%;"><?=chr(65+$ColumnsCount).$RowsCount;?></button>
                                </td>
                            <?php 
                                    endfor;
                                } 
                            ?> 
                        </tr>
                    <?php 
                            endfor;
                        } 
                    ?>
                </table>
                
            </div>
        </div>
    <!-- Freezer Body -->
    </div>
</div>