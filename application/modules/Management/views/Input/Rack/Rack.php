<div class="card">
    <div class="card-header">
        <h3 class="title-2 m-b-40"><?=(isset($Site_Title) && $Site_Title != '' ? $Site_Title : 'Input');?></h3>
    </div>
    <div class="card-body">
    <!-- Freezer Body -->
        <div class="row">
            <?php 
            if(isset($Data['InputData']) && $Data['InputData']->num_rows() > 0){
                foreach ($Data['InputData']->result() as $Row) : 
            ?>
                <div class="col-2 text-center" style="margin: 0 auto;max-width:100%;">
                    <a href="<?=base_url('Management/Input/RackFloor/').$Row->RACK_ID;?>">
                        <?=assets_img('images/icon/management/rack.png',array('width'=>'70%','alt'=>$Row->RACK_EDIT));?>
                        <h3 class="text-center p-t-10 p-b-20"><?=$Row->RACK_EDIT;?></h3>
                    </a>
                </div>

            <?php 
                endforeach;
            }
            ?>
        </div>
    <!-- Freezer Body -->
    </div>
</div>