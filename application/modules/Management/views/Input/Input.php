<div class="card">
    <div class="card-header">
        <h3 class="title-2 m-b-40"><?=(isset($Site_Title) && $Site_Title != '' ? $Site_Title : 'Input');?></h3>
    </div>
    <div class="card-body">
    <!-- Freezer Body -->
        <?php 
        if(isset($Data['InputData']['FreezerData']) && $Data['InputData']['FreezerData']->num_rows() > 0){
            foreach ($Data['InputData']['FreezerData']->result() as $Row) : 
        ?>
            <div id="ACCORDION">
                <div class="card">
                    <div class="card-header" id="heading-1">
                        <h5 class="mb-0">
                            <a role="button" data-toggle="collapse" href="#FREEZER_<?=$Row->FREEZER_ID;?>" aria-expanded="true" aria-controls="collapse-1">
                            <?=assets_img('images/icon/management/freezer.png',array('class'=>'ac_icon fz_icon','alt'=>$Row->FREEZER_NAME));?>
                              <?=$Row->FREEZER_NAME;?>
                            </a>
                        </h5>
                    </div>
                    <div id="FREEZER_<?=$Row->FREEZER_ID;?>" class="collapse show" data-parent="#ACCORDION" aria-labelledby="heading-1">
                        <div class="card-body">  
        <!-- Freezer Floor Body -->
            <?php 
            $Count_FZ_FL = 0;
            if(isset($Data['InputData']['FreezerFloorData']) && $Data['InputData']['FreezerFloorData']->num_rows() > 0){
                foreach ($Data['InputData']['FreezerFloorData']->result() as $Row2nd) : 
            ?>
                <?php 
                if($Row2nd->FREEZER_ID == $Row->FREEZER_ID) :
                    $Count_FZ_FL++; ?>
                    <div id="ACCORDION_FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>">
                        <div class="card">
                            <div class="card-header" id="heading-1">
                                <h5 class="mb-0">
                                    <a role="button" data-toggle="collapse" href="#FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" aria-expanded="false" aria-controls="collapse-1">
                                        <?=assets_img('images/icon/management/freezer_floor.png',array('class'=>'ac_icon fz_f_icon','alt'=>$Row2nd->FREEZER_FLOOR_EDIT));?>  <?=$Row2nd->FREEZER_FLOOR_EDIT;?>
                                    </a>
                                </h5>
                            </div>
                            <div id="FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" class="collapse" data-parent="#ACCORDION_FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" aria-labelledby="heading-1">
                                <div class="card-body">
            <!-- Rack Body -->
                <?php 
                $Count_R = 0;
                if(isset($Data['InputData']['RackData']) && $Data['InputData']['RackData']->num_rows() > 0){
                    foreach($Data['InputData']['RackData']->result() as $Row3rd) : 
                ?>
                    <?php 
                    if($Row3rd->FREEZER_FLOOR_ID == $Row2nd->FREEZER_FLOOR_ID) :
                        $Count_R++; ?>
                        <div id="ACCORDION_RACK_<?=$Row3rd->RACK_ID;?>">
                            <div class="card">
                                <div class="card-header" id="heading-1">
                                    <h5 class="mb-0">
                                        <a role="button" data-toggle="collapse" href="#RACK_<?=$Row3rd->RACK_ID;?>" aria-expanded="false" aria-controls="collapse-1">
                                            <?=assets_img('images/icon/management/rack.png',array('class'=>'ac_icon r_icon','alt'=>$Row3rd->RACK_EDIT));?>  <?=$Row3rd->RACK_EDIT;?>
                                        </a>
                                    </h5>
                                </div>
                                <div id="RACK_<?=$Row3rd->RACK_ID;?>" class="collapse" data-parent="#ACCORDION_RACK_<?=$Row3rd->RACK_ID;?>" aria-labelledby="heading-1">
                                    <div class="card-body">  
                <!-- Rack Floor Body -->                                    
                    <?php 
                    $COUNT_R_FL = 0;
                    if(isset($Data['InputData']['RackFloorData']) && $Data['InputData']['RackFloorData']->num_rows() > 0){
                        foreach($Data['InputData']['RackFloorData']->result() as $Row4th) : 
                    ?>
                        <?php 
                        if($Row4th->RACK_ID == $Row3rd->RACK_ID) : 
                            $COUNT_R_FL++; ?>
                            <div id="ACCORDION_RACK_FLOOR_<?=$Row4th->RACK_FLOOR_ID;?>">
                                <div class="card">
                                    <div class="card-header" id="heading-1">
                                        <h5 class="mb-0">
                                            <div class="row">
                                                <div class="col">
                                                    <a role="button" data-toggle="collapse" href="#RACK_FLOOR_<?=$Row4th->RACK_FLOOR_ID;?>" aria-expanded="false" aria-controls="collapse-1">
                                                        <?=assets_img('images/icon/management/rack_floor.png',array('class'=>'ac_icon r_f_icon','alt'=>$Row4th->RACK_FLOOR_EDIT));?>  <?=$Row4th->RACK_FLOOR_EDIT;?>
                                                    </a>
                                                </div>
                                                <div class="col text-right">
                                                    <button class="btn btn-success sm-1" modal-add="box" data-freezerid="<?=$Row->FREEZER_ID;?>" data-freezerfloorid="<?=$Row2nd->FREEZER_FLOOR_ID;?>" data-rackid="<?=$Row3rd->RACK_ID;?>" data-rackfloorid="<?=$Row4th->RACK_FLOOR_ID;?>">
                                                        Add Box
                                                    </button>
                                                </div>
                                            </div>
                                        </h5>
                                    </div>
                                    <div id="RACK_FLOOR_<?=$Row4th->RACK_FLOOR_ID;?>" class="collapse" data-parent="#ACCORDION_RACK_FLOOR_<?=$Row4th->RACK_FLOOR_ID;?>" aria-labelledby="heading-1">
                                        <div class="card-body">
                    <!-- Box Body -->
                        <?php 
                        $COUNT_BOX = 0;
                        if(isset($Data['InputData']['FreezerBoxData']) && $Data['InputData']['FreezerBoxData']->num_rows() > 0){
                            foreach($Data['InputData']['FreezerBoxData']->result() as $Row5th) : 
                        ?>
                            <?php $FREEZER_BOX_LINK = base_url('Management/Input/BoxInput/').$Row5th->BOX_BARCODE;?>
                            <?php 
                            if($Row5th->RACK_FLOOR_ID == $Row4th->RACK_FLOOR_ID) :
                                $COUNT_BOX++; ?>
                                <div class="row">
                                    <div class="col">
                                        <button class="btn btn-danger sm-1" modal-del="box" data-id="<?=$Row5th->BOX_BARCODE;?>">
                                            Delete
                                        </button>
                                        <a href="<?=$FREEZER_BOX_LINK;?>">
                                            <?=assets_img('images/icon/management/box.png',array('class'=>'ac_icon box_icon','alt'=>$Row5th->BOX_NAME));?>  <?=$Row5th->BOX_NAME;?>                                    
                                        </a>
                                    </div>
                                </div>
                                <br>
                            <?php endif ?>
                        <?php 
                            endforeach;
                        }
                        echo ($COUNT_BOX == 0 ? 'No Box In This Rack Floor <br>' : '');
                        ?>
                    <!-- Box Body -->

                                        </div>
                                    </div>  
                                </div>
                            </div>
                        <?php endif ?>
                    <?php 
                        endforeach;
                    }
                    echo ($COUNT_R_FL == 0 ? 'No Floor In This Rack <br>' : '');
                    ?>
                <!-- Rack Floor Body -->
                                    </div>
                                </div>  
                            </div>
                        </div>
                    <?php endif ?>
                <?php 
                    endforeach;
                }
                echo ($Count_R == 0 ? 'No Rack In This Freezer Floor <br>' : '');
                ?>
            <!-- Rack Body -->
                                </div>
                            </div>  
                        </div>
                    </div>
                <?php endif ?>
            <?php 
                endforeach;
            }
            echo ($Count_FZ_FL == 0 ? 'No Floor In This Freezer <br>' : '');
            ?>
        <!-- Freezer Floor Body -->
                        </div>
                    </div>  
                </div>
            </div>

        <?php 
            endforeach;
        }
        ?>
    <!-- Freezer Body -->
    </div>
</div>