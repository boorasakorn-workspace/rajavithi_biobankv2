<div class="modal fade" id="master_edit-modal" tabindex="-1" role="dialog" aria-labelledby="master_edit-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Edit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterEdit', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'id_target',
                            'id' => 'id_target',
                        );
                        $target_input = array(
                            'type' => 'hidden',
                            'name' => 'add_target',
                            'id' => 'add_target',
                        );
                    ?>
                    <?=form_input($method_input);?>
                    <?=form_input($hidden_input);?>
                    <?=form_input($target_input);?>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('input_name','','Name');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="master_edit_submit" data-dismiss="modal" form_submit>Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="master_del-modal" tabindex="-1" role="dialog" aria-labelledby="master_del-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterDel', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'del_id',
                            'id' => 'del_id',
                        );
                        $target_input = array(
                            'type' => 'hidden',
                            'name' => 'del_target',
                            'id' => 'del_target',
                        );
                    ?>
                    <?=form_input($hidden_input);?>
                    <?=form_input($target_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_target"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>