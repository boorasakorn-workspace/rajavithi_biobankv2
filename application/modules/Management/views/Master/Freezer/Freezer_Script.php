<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('click','[modal-add]',function(){
			$('#master_edit-modal .modal-title').text('Add');
			$('#master_edit-modal input#method').val('add');
			var tAttr = $(this).attr('modal-add');
			var appendTitle = '';
			var add_target = '';
			var parentID = '';
			switch(tAttr) {
			case 'freezer':
				appendTitle = 'Freezer';
				add_target = 'freezer';
				parentID = '';				
				break;
			case 'freezerfloor':
				appendTitle = 'Freezer Floor';
				add_target = 'freezerfloor';
				parentID = $(this).data('parentid');
				break;
			case 'rack':
				appendTitle = 'Rack';
				add_target = 'rack';
				parentID = $(this).data('parentid');
				break;
			case 'rackfloor':
				appendTitle = 'Rack Floor';
				add_target = 'rackfloor';
				parentID = $(this).data('parentid');
				break;
			}
			$('#master_edit-modal input#id_target').val(parentID);
			$('#master_edit-modal input#add_target').val(add_target);
			$('#master_edit-modal .modal-title').text('Add'+' '+appendTitle);
			$('#master_edit-modal #master_edit_submit').text('Add'+' '+appendTitle);
			$('#master_edit-modal').modal('show');
		});

		$(document).on('click','[modal-edit]',function(){
			$('#master_edit-modal .modal-title').text('Edit');
			$('#master_edit-modal input#method').val('edit');
			var tID = $(this).data('id');
			var tAttr = $(this).attr('modal-edit');
			var appendTitle = '';
			var add_target = '';
			switch(tAttr) {
			case 'freezer':
				appendTitle = 'Freezer';
				add_target = 'freezer';
				break;
			case 'freezerfloor':
				appendTitle = 'Freezer Floor';
				add_target = 'freezerfloor';
				break;
			case 'rack':
				appendTitle = 'Rack';
				add_target = 'rack';
				break;
			case 'rackfloor':
				appendTitle = 'Rack Floor';
				add_target = 'rackfloor';				
				break;
			}
			$('#master_edit-modal input#id_target').val(tID);
			$('#master_edit-modal input#add_target').val(add_target);
			$('#master_edit-modal .modal-title').text('Edit'+' '+appendTitle);
			$('#master_edit-modal #master_edit_submit').text('Edit'+' '+appendTitle);
			$('#master_edit-modal').modal('show');
		});

		$(document).on('click','[modal-del]',function(){
			$('#master_del-modal .modal-title').text('Delete');
			var tID = $(this).data('id');
			var tAttr = $(this).attr('modal-del');
			var appendTitle = '';
			var del_target = '';
			switch(tAttr) {
			case 'freezer':
				appendTitle = 'Freezer';
				del_target = 'freezer';
				break;
			case 'freezerfloor':
				appendTitle = 'Freezer Floor';
				del_target = 'freezerfloor';
				break;
			case 'rack':
				appendTitle = 'Rack';
				del_target = 'rack';
				break;
			case 'rackfloor':
				appendTitle = 'Rack Floor';
				del_target = 'rackfloor';				
				break;
			}
			$('#master_del-modal input#del_id').val(tID);
			$('#master_del-modal input#del_target').val(del_target);
			$('#master_del-modal .modal-title').text('Delete'+' '+appendTitle);
			$('#master_del-modal').modal('show');
		});


	})
</script>