<div class="card">
    <div class="card-header">
        <h3 class="title-2 m-b-40">
            <div class="row">
                <div class="col">
                    <?=(isset($Site_Title) && $Site_Title != '' ? $Site_Title : 'Input');?>
                </div>
                <div class="col text-right">
                    <button class="btn btn-success sm-1" modal-add="freezer">
                        Add Freezer
                    </button>
                </div>
            </div>
        </h3>
    </div>
    <div class="card-body">
    <!-- Freezer Body -->
        <?php 
        if(isset($Data['InputData']['FreezerData']) && $Data['InputData']['FreezerData']->num_rows() > 0){
            foreach ($Data['InputData']['FreezerData']->result() as $Row) : 
        ?>
            <div id="ACCORDION">
                <div class="card">
                    <div class="card-header" id="heading-1">
                        <h5 class="mb-0">
                            <div class="row">
                                <div class="col">
                                    <a role="button" data-toggle="collapse" href="#FREEZER_<?=$Row->FREEZER_ID;?>" aria-expanded="true" aria-controls="collapse-1">
                                    <?=assets_img('images/icon/management/freezer.png',array('class'=>'ac_icon fz_icon','alt'=>$Row->FREEZER_NAME));?>
                                      <?=$Row->FREEZER_NAME;?>
                                    </a>
                                </div>
                                <div class="col text-right">
                                    <button class="btn btn-success sm-1" modal-add="freezerfloor" data-parentid="<?=$Row->FREEZER_ID;?>">
                                        Add Floor
                                    </button>
                                    <button class="btn btn-warning sm-1" modal-edit="freezer" data-id="<?=$Row->FREEZER_ID;?>">
                                        Edit
                                    </button>
                                    <button class="btn btn-danger sm-1" modal-del="freezer" data-id="<?=$Row->FREEZER_ID;?>">
                                        Del
                                    </button>
                                </div>
                            </div>
                        </h5>
                    </div>
                    <div id="FREEZER_<?=$Row->FREEZER_ID;?>" class="collapse show" data-parent="#ACCORDION" aria-labelledby="heading-1">
                        <div class="card-body">  
        <!-- Freezer Floor Body -->
            <?php 
            $Count_FZ_FL = 0;
            if(isset($Data['InputData']['FreezerFloorData']) && $Data['InputData']['FreezerFloorData']->num_rows() > 0){
                foreach ($Data['InputData']['FreezerFloorData']->result() as $Row2nd) : 
            ?>
                <?php 
                if($Row2nd->FREEZER_ID == $Row->FREEZER_ID) :
                    $Count_FZ_FL++; ?>
                    <div id="ACCORDION_FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>">
                        <div class="card">
                            <div class="card-header" id="heading-1">
                                <h5 class="mb-0">
                                    <div class="row">
                                        <div class="col">
                                            <a role="button" data-toggle="collapse" href="#FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" aria-expanded="false" aria-controls="collapse-1">
                                                <?=assets_img('images/icon/management/freezer_floor.png',array('class'=>'ac_icon fz_f_icon','alt'=>$Row2nd->FREEZER_FLOOR_EDIT));?>  <?=$Row2nd->FREEZER_FLOOR_EDIT;?>
                                            </a>
                                        </div>
                                        <div class="col text-right">
                                            <button class="btn btn-success sm-1" modal-add="rack" data-parentid="<?=$Row2nd->FREEZER_FLOOR_ID;?>">
                                                Add Rack
                                            </button>
                                            <button class="btn btn-warning sm-1" modal-edit="freezerfloor" data-id="<?=$Row2nd->FREEZER_FLOOR_ID;?>">
                                                Edit
                                            </button>
                                            <button class="btn btn-danger sm-1" modal-del="freezerfloor" data-id="<?=$Row2nd->FREEZER_FLOOR_ID;?>">
                                                Del
                                            </button>
                                        </div>
                                    </div>                                    
                                </h5>
                            </div>
                            <div id="FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" class="collapse" data-parent="#ACCORDION_FREEZER_FLOOR_<?=$Row2nd->FREEZER_FLOOR_ID;?>" aria-labelledby="heading-1">
                                <div class="card-body">
            <!-- Rack Body -->
                <?php 
                $Count_R = 0;
                if(isset($Data['InputData']['RackData']) && $Data['InputData']['RackData']->num_rows() > 0){
                    foreach($Data['InputData']['RackData']->result() as $Row3rd) : 
                ?>
                    <?php 
                    if($Row3rd->FREEZER_FLOOR_ID == $Row2nd->FREEZER_FLOOR_ID) :
                        $Count_R++; ?>
                        <div id="ACCORDION_RACK_<?=$Row3rd->RACK_ID;?>">
                            <div class="card">
                                <div class="card-header" id="heading-1">
                                    <h5 class="mb-0">
                                        <div class="row">
                                            <div class="col">
                                                <a role="button" data-toggle="collapse" href="#RACK_<?=$Row3rd->RACK_ID;?>" aria-expanded="false" aria-controls="collapse-1">
                                                    <?=assets_img('images/icon/management/rack.png',array('class'=>'ac_icon r_icon','alt'=>$Row3rd->RACK_EDIT));?>  <?=$Row3rd->RACK_EDIT;?>
                                                </a>
                                            </div>
                                            <div class="col text-right">
                                                <button class="btn btn-success sm-1" modal-add="rackfloor" data-parentid="<?=$Row3rd->RACK_ID;?>">
                                                    Add Floor
                                                </button>
                                                <button class="btn btn-warning sm-1" modal-edit="rack" data-id="<?=$Row3rd->RACK_ID;?>">
                                                    Edit
                                                </button>
                                                <button class="btn btn-danger sm-1" modal-del="rack" data-id="<?=$Row3rd->RACK_ID;?>">
                                                    Del
                                                </button>
                                            </div>
                                        </div>
                                    </h5>
                                </div>
                                <div id="RACK_<?=$Row3rd->RACK_ID;?>" class="collapse" data-parent="#ACCORDION_RACK_<?=$Row3rd->RACK_ID;?>" aria-labelledby="heading-1">
                                    <div class="card-body">  
                <!-- Rack Floor Body -->                                    
                    <?php 
                    $COUNT_R_FL = 0;
                    if(isset($Data['InputData']['RackFloorData']) && $Data['InputData']['RackFloorData']->num_rows() > 0){
                        foreach($Data['InputData']['RackFloorData']->result() as $Row4th) : 
                    ?>
                        <?php 
                        if($Row4th->RACK_ID == $Row3rd->RACK_ID) : 
                            $COUNT_R_FL++; ?>
                            <div class="row">
                                <div class="col">          
                                    <button class="btn btn-warning sm-1" modal-edit="rackfloor" data-id="<?=$Row4th->RACK_FLOOR_ID;?>">
                                        Edit
                                    </button>
                                    <button class="btn btn-danger sm-1" modal-del="rackfloor" data-id="<?=$Row4th->RACK_FLOOR_ID;?>">
                                        Del
                                    </button> 
                                    <?=assets_img('images/icon/management/rack_floor.png',array('class'=>'ac_icon r_f_icon','alt'=>$Row4th->RACK_FLOOR_EDIT));?>   
                                    <?=$Row4th->RACK_FLOOR_EDIT;?>
                                </div>                                
                            </div>
                        <?php endif ?>
                    <?php 
                        endforeach;
                    }
                    echo ($COUNT_R_FL == 0 ? 'No Floor In This Rack <br>' : '');
                    ?>
                <!-- Rack Floor Body -->
                                    </div>
                                </div>  
                            </div>
                        </div>
                    <?php endif ?>
                <?php 
                    endforeach;
                }
                echo ($Count_R == 0 ? 'No Rack In This Freezer Floor <br>' : '');
                ?>
            <!-- Rack Body -->
                                </div>
                            </div>  
                        </div>
                    </div>
                <?php endif ?>
            <?php 
                endforeach;
            }
            echo ($Count_FZ_FL == 0 ? 'No Floor In This Freezer <br>' : '');
            ?>
        <!-- Freezer Floor Body -->
                        </div>
                    </div>  
                </div>
            </div>

        <?php 
            endforeach;
        }
        ?>
    <!-- Freezer Body -->
    </div>
</div>