<div class="card rounded-0" id="register-form">
    <div class="card-header">
		<h3>Box</h3>
	</div>
</div>
<div class="card-body">
	<div class="row">
		<div class="col"><h4>Box</h4></div>
		<div class="col text-right">
			<button type="button" class="btn btn-success mb-1" add-box> New Box </button>
		</div>
	</div>
	<div class="container-fluid">
		<table id="BoxTable" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
				    <th style="white-space: nowrap;">#</th>
				    <th style="white-space: nowrap;">Barcode</th>  
                    <th style="white-space: nowrap;">Name</th>
                    <th style="white-space: nowrap;">Size</th>
                    <th style="white-space: nowrap;">Row</th>
				    <th style="white-space: nowrap;">Column</th>
				    <th style="white-space: nowrap;">TubeCount</th>
				    <th style="white-space: nowrap;">Location</th>
				    <th style="white-space: nowrap;">Manage</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($Data['InputData']['Box'] as $key => $Box): ?>
					<tr>
					    <td style="white-space: nowrap;"><?=$key;?></td>
					    <td style="white-space: nowrap;"><?=$Box->BOX_BARCODE;?></td>  
	                    <td style="white-space: nowrap;"><?=$Box->BOX_NAME;?></td>
					    <td style="white-space: nowrap;"><?=$Box->BOX_ROW*$Box->BOX_COLUM;?></td>
					    <td style="white-space: nowrap;"><?=$Box->BOX_ROW;?></td>
	                    <td style="white-space: nowrap;"><?=$Box->BOX_COLUM;?></td>
	                    <td style="white-space: nowrap;">
	                    	<?=$Box->COUNT_TUBE;?>/<?=$Box->BOX_ROW*$Box->BOX_COLUM;?> 
	                    	<?=($Box->COUNT_TUBE == $Box->BOX_ROW*$Box->BOX_COLUM ? 'FULL' : '');?>
	                    </td>
	                    <td style="white-space: nowrap;">
	                    	<?php 
	                    		$BoxLocate = $Data['InputData']['BoxLocate'];
		                    	foreach($BoxLocate as $locate_key => $locate_box): 
		                    		if($locate_box->BOX_BARCODE == $Box->BOX_BARCODE){
		                    			$locatetext = "";
		                    			$locate_loop = array(
		                    				'freezer' => array(
		                    					'id' => (isset($locate_box->FREEZER_ID) ? $locate_box->FREEZER_ID:''),
		                    					'name' => (isset($locate_box->FREEZER_NAME) ? $locate_box->FREEZER_NAME:''),
		                    					'href' => 'Management/Input/FreezerFloor/',
		                    				),
		                    				'freezer_floor' => array(
		                    					'id' => (isset($locate_box->FREEZER_FLOOR_ID) ? $locate_box->FREEZER_FLOOR_ID:''),
		                    					'name' => (isset($locate_box->FREEZER_FLOOR_NAME) ? $locate_box->FREEZER_FLOOR_NAME:''),
		                    					'href' => 'Management/Input/Rack/',
		                    					'addition' => '>',
		                    				),
		                    				'rack' => array(
		                    					'id' => (isset($locate_box->RACK_ID) ? $locate_box->RACK_ID:''),
		                    					'name' => (isset($locate_box->RACK_NAME) ? $locate_box->RACK_NAME:''),
		                    					'href' => 'Management/Input/RackFloor/',
		                    					'addition' => '>',
		                    				),
		                    				'rack_floor' => array(
		                    					'id' => (isset($locate_box->RACK_FLOOR_ID) ? $locate_box->RACK_FLOOR_ID:''),
		                    					'name' => (isset($locate_box->RACK_FLOOR_NAME) ? $locate_box->RACK_FLOOR_NAME:''),
		                    					'href' => 'Management/Input/Box/',
		                    					'addition' => '>',
		                    				),
		                    			);
		                    			foreach($locate_loop as $loop_result):
		                    				if($loop_result['id'] != NULL && $loop_result['id'] != ''){
		                    					$locatetext .= (isset($loop_result['addition']) ? $loop_result['addition']:'' );
			                    				$locatetext .= '<a href="'.base_url($loop_result['href'].$loop_result['id']).'">';
			                    				$locatetext .= '<button class="btn btn-primary">';
			                    				$locatetext .= $loop_result['name'];
			                    				$locatetext .= '</button>';
			                    				$locatetext .= '</a>';
			                    			}
		                    			endforeach;
		                    			echo $locatetext;
		                    		}
			                    endforeach;
                    		?>	                    		
	                    </td>
					    <td style="white-space: nowrap;">
					    	<a href="<?=base_url('Management/Input/BoxInput/'.$Box->BOX_BARCODE);?>">
								<button type="button" class="btn btn-success">Box</button>
					    	</a>
							<button type="button" class="btn btn-warning" edit-box="<?=$Box->BOX_BARCODE;?>">Edit</button>
							<button type="button" class="btn btn-danger" del-box="<?=$Box->BOX_BARCODE;?>">Delete</button>
					    </td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>