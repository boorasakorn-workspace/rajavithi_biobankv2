<script type="text/javascript">
	$(document).ready(function(){
		var UserTable = $('#BoxTable').DataTable({
				ordering: false,
				scrollY: true,
				scrollX: true,
				paging: true,
				pageLength: 10,
				dom: 'frtp',
			});

		$('#management_content').on('resize', function () {
			UserTable.draw();
		});
		
		$(document).on('click','[new-sample]',function(){
			$('#method').val('add');
			$('#sample_edit_submit').text('Add');
			$('#modal-title').text('Add Sample');
			$('#edit_sample-modal').modal('show');		
		});

		$(document).on('click','[add-box]',function(){
			$('.modal-body input').val('');
			var now = new Date();
			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day+'/'+month+'/'+now.getFullYear();
			
			h = now.getHours(),
			m = now.getMinutes();
			if(h < 10) h = '0' + h; 
			if(m < 10) m = '0' + m; 

			$('#created_date').val(today);
			$('#created_time').val(h + ':' + m);
			$('#method').val('add');
			$('#box_edit_submit').text('Add');
			$('#edit_box-modal #modal-title').text('Add Box');
			$('#edit_box-modal').modal('show');
		});

		$(document).on('click','[edit-box]',function(){
			var BoxBarcode = $(this).attr('edit-box');
			$.get( "<?=base_url('Service/JSON_ListData/Box/');?>"+BoxBarcode, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				var loop_input = {
					'BOX_BARCODE':'box_barcode',
					'BOX_NAME':'box_name',
					'BOX_ROW':'box_row',
					'BOX_COLUM':'box_column',
					'BOX_DATE':'created_date',
					'BOX_TIME':'created_time',
				}
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+loop_input[key]).length ){
						$('#'+loop_input[key]).val(data['result'][key]);					
					}					
				});

				$('#method').val('edit');
				$('#box_edit_submit').text('Edit');
				$('#edit_box-modal #modal-title').text('Edit Box');
				$('#edit_box-modal').modal('show');
			});
		});

		$(document).on('click','[del-box]',function(){
			var BoxBarcode = $(this).attr('del-box');
			var targetBarcode = BoxBarcode;
			$('#span_box_barcode').text(targetBarcode);
			$('#del_box_barcode').val(targetBarcode);
			$('#del_box-modal').modal('show');
		});

	})
</script>