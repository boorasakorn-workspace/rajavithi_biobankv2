<div class="modal fade" id="edit_box-modal" tabindex="-1" role="dialog" aria-labelledby="edit_box-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Edit Box</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/EditBox', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                    ?>
                    <?=form_input($method_input);?>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('box_barcode','','Box Barcode');?>
                        </div>  
                        <div class="col">
                            <?=form_group_input('box_name','','Box Name');?>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('Date'), 'created_date');
                                    echo datepicker_th('created_date');
                                ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('Time'), 'created_time');
                                ?>
                                <input type="time" class="form-control" id="created_time" name="created_time" value="">
                            </div>
                        </div>
                        <div class="col-3">
                            <?=form_group_input('box_row','','Row Count');?>
                        </div>  
                        <div class="col-3">
                            <?=form_group_input('box_column','','Column Count');?>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="box_edit_submit" data-dismiss="modal" form_submit>Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="del_box-modal" tabindex="-1" role="dialog" aria-labelledby="del_box-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Box</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/DeleteBox', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'box_barcode',
                            'id' => 'del_box_barcode',
                        );
                    ?>
                    <?=form_input($hidden_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_box_barcode"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>