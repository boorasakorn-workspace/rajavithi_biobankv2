<div class="card rounded-0" id="register-form">
    <div class="card-header">
		<div class="row">
			<div class="col"><h3>Search</h3></div>
		</div>
	</div>
</div>
<div class="card-body">
	<div class="container-fluid">
		<table id="SampleTable" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
				    <th style="white-space: nowrap;">#</th>
				    <th style="white-space: nowrap;">Barcode</th>  
				    <th style="white-space: nowrap;">HN</th>  
                    <th style="white-space: nowrap;">Fullname</th>
                    <th style="white-space: nowrap;">Birthdate</th>
				    <th style="white-space: nowrap;">Age</th>
				    <th style="white-space: nowrap;">Gender</th>
                    <th style="white-space: nowrap;">Type</th>
                    <th style="white-space: nowrap;">DataStatus</th>
				    <th style="white-space: nowrap;">Location</th>
				    <th style="white-space: nowrap;">Manage</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($Data['Sample']->result() as $key => $Sample): ?>	
			    	<?php if( intval($Sample->BOXADD_STATUS) == 1 ){ ?>
					<tr>
					    <td style="white-space: nowrap;"><?=$key;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_SID;?></td>  
	                    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_HN;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_FULLNAME?></td>  
	                    <td style="white-space: nowrap;"><?=convert_date_format($Sample->SAMPLE_HBD);?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_AGE;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_GENDER;?></td>
	                    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_TYPE;?></td>
	                    <td style="white-space: nowrap;">
					    	<?php if( intval($Sample->DATA_STATUS) == 1 ){ ?>
								<button type="button" class="btn btn-sm btn-success" info-sample="<?=$Sample->SAMPLE_SID;?>">Info</button>
						    <?php }else{ 
						    	echo "Not Exists";
						    } ?>	                    		
	                    </td>
					    <td style="white-space: nowrap;">					    	
					    	<?php if( intval($Sample->BOXADD_STATUS) == 1 ){ ?>
								<button type="button" class="btn btn-sm btn-primary" tube-location="<?=$Sample->SAMPLE_SID;?>">Location</button>
						    <?php }else{ 
						    	echo "Not Input";
						    } ?>
					    </td>
					    <td style="white-space: nowrap;">
					    	<?php if( intval($Sample->DATA_STATUS) == 1 ){ ?>
								<button type="button" class="btn btn-sm btn-warning" edit-sample="<?=$Sample->SAMPLE_SID;?>">Edit</button>
								<button type="button" class="btn btn-sm btn-danger" del-sample="<?=$Sample->SAMPLE_SID;?>">Delete</button>
						    <?php }else{ ?>
								<button type="button" class="btn btn-sm btn-success" add-sample="<?=$Sample->SAMPLE_SID;?>">Add</button>
						    <?php } ?>
					    </td>
					</tr>
					<?php } ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>