<script type="text/javascript">
	function removeDuplicateRows($table,callback){
	    function getVisibleRowText($row){
	        return $row.find('td:visible').not('td:first').text().toLowerCase();
	    }

	    $table.find('tr').each(function(index, row){
	        var $row = $(row);
	        $row.nextAll('tr').each(function(index, next){
	            var $next = $(next);
	            if(getVisibleRowText($next) == getVisibleRowText($row))
	                $next.remove();
	        })
	    });
	    if(callback){
	    	callback;
	    }
	}

	$(document).ready(function(){
		removeDuplicateRows($('#SampleTable'));
		var UserTable = $('#SampleTable').DataTable({
				ordering: false,
				scrollY: true,
				scrollX: true,
				paging: true,
				pageLength: 10,
				dom: 'frtp',
			});

		$('#management_content').on('resize', function () {
			UserTable.draw();
		});
		
		$(document).on('click','[tube-location]',function(){
			var SampleID = $(this).attr('tube-location');
			$.get( "<?=base_url('Service/TubeLocate/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length){
						$('#'+key).html(data['result'][key]);					
					}					
				});

				let btn_loop = {
					'freezer':{
						'id':'id_freezer',
						'button':'#freezer_button',
						'href':'<?=base_url('Management/Input/FreezerFloor/');?>',	
					},
					'freezer_floor':{
						'id':'id_freezer_floor',
						'button':'#freezer_floor_button',
						'href':'<?=base_url('Management/Input/Rack/');?>',	
					},
					'rack':{
						'id':'id_rack',
						'button':'#rack_button',
						'href':'<?=base_url('Management/Input/RackFloor/');?>',	
					},
					'rack_floor':{
						'id':'id_rack_floor',
						'button':'#rack_floor_button',
						'href':'<?=base_url('Management/Input/Box/');?>',	
					},
					'box':{
						'id':'barcode_box',
						'button':'#box_button',
						'href':'<?=base_url('Management/Input/BoxInput/');?>',	
					},
				};

				Object.keys(btn_loop).forEach(function (key) {
					if(data['result'][btn_loop[key]['id']] != null && data['result'][btn_loop[key]['id']] != ''){
						var href_target = btn_loop[key]['href'];
						var append_target = data['result'][btn_loop[key]['id']];
						$(btn_loop[key]['button']).attr('href',href_target+append_target);
						$(btn_loop[key]['button']+' button').attr('disabled',false);
					}else{
						$(btn_loop[key]['button']+' button').attr('disabled',true);
					}				
				});

				$('#tube_locate-modal').modal('show');
			});			
		});
		
		$(document).on('click','[info-sample]',function(){
			var SampleID = $(this).attr('info-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#info_'+key).length ){
						$('#info_'+key).val(data['result'][key]);					
					}					
				});
				$('#info_sample-modal').modal('show');
			});
		});
		
		$(document).on('click','[add-sample]',function(){
			var SampleID = $(this).attr('add-sample');
			$.get( "<?=base_url('Service/AnuData/Sample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length ){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#method').val('add');
				$('#sample_edit_submit').text('Add');
				$('#modal-title').text('Add Sample');
				$('#edit_sample-modal').modal('show');
			});			
		});

		$(document).on('click','[edit-sample]',function(){
			var SampleID = $(this).attr('edit-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#method').val('edit');
				$('#sample_edit_submit').text('Edit');
				$('#modal-title').text('Edit Sample');
				$('#edit_sample-modal').modal('show');
			});
		});

		$(document).on('click','[del-sample]',function(){
			var SampleID = $(this).attr('del-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				var targetFullname = data['result']['prefix'] + ' ' + data['result']['firstname'] + ' ' + data['result']['lastname'];
				$('#span_fullname').text(targetFullname);
				$('#del_sample_sid').val(data['result']['scanln']);
				$('#del_sample-modal').modal('show');
			});
		});

	})
</script>