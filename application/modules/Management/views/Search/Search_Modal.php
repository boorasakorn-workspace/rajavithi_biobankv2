<div class="modal fade" id="info_sample-modal" tabindex="-1" role="dialog" aria-labelledby="info_sample-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Sample Info</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">
                        <?=form_group_input('info_scanln','','SCAN LN');?>
                    </div>  
                    <div class="col">
                        <div class="form-group">
                            <?php
                                echo form_label(ucfirst('In Date'), 'info_indate');
                                echo datepicker_th('info_indate');
                            ?>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <?=form_group_input('info_intime','','In Time');?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <?=form_group_input('info_prefix','','Prefix');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_firstname','','Firstname');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_lastname','','Lastname');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?=form_group_input('info_hn','','HN');?>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <?php
                                echo form_label(ucfirst('birthdate'), 'info_birthdate');
                                echo datepicker_th('info_birthdate');
                            ?>
                        </div>
                    </div>
                    <div class="col-2">
                        <?=form_group_input('info_age','','Age');?>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <?=form_group_input('info_gender','','Gender');?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <?=form_group_input('info_sample_type','','Type Of Specimen');?>
                        </div>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_pjowner','','Project Owner');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_consent','','Consent From');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?=form_group_input('info_pjname','','Project Name');?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tube_locate-modal" tabindex="-1" role="dialog" aria-labelledby="tube_locate-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="info_sample_name_title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <H4 class="infomationH">Sample Info</H4>
                <div class="row">
                    <div class="col-md-4" style="display:none;">
                        <p class="infomationP">Sample Name</p>
                        <div name="info_sample_name" id="info_sample_name" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                    </div>
                    <div class="col-md-4">
                        <p class="infomationP">Tube Barcode</p>
                        <div name="info_sample_sid" id="info_sample_sid" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                    </div>
                </div>
                <H5 class="infomationH">Tube Location </H5>
                <div class="row">
                    <div class="col-md-6">
                        <p class="infomationP">
                            <a id="freezer_button" href="<?=base_url('Management/Input/FreezerFloor/');?>"><button class="btn btn-warp btn-sm">></button></a>
                            Location Freezer                             
                        </p>
                        <div name="locate_freezer" id="locate_freezer" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>                        
                    </div>
                    <div class="col-md-6">
                        <p class="infomationP">
                            <a id="freezer_floor_button" href="<?=base_url('Management/Input/Rack/');?>"><button class="btn btn-warp btn-sm">></button></a>
                            Location Freezer Floor                             
                        </p>
                        <div name="locate_freezer_floor" id="locate_freezer_floor" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="infomationP">
                            <a id="rack_button" href="<?=base_url('Management/Input/RackFloor/');?>"><button class="btn btn-warp btn-sm">></button></a>
                            Location Rack 
                        </p>
                        <div name="locate_rack" id="locate_rack" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                        
                    </div>
                    <div class="col-md-6">
                        <p class="infomationP">
                            <a id="rack_floor_button" href="<?=base_url('Management/Input/Box/');?>"><button class="btn btn-warp btn-sm">></button></a> 
                            Location Rack Floor 
                        </p>
                        <div name="locate_rack_floor" id="locate_rack_floor" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <p class="infomationP">
                            <a id="box_button" href="<?=base_url('Management/Input/BoxInput/');?>"><button class="btn btn-warp btn-sm">></button></a>
                            Location Box 
                        </p>
                        <div name="locate_box" id="locate_box" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                    </div>
                    <div class="col-md-6">
                        <p class="infomationP">Location Box Well </p>
                        <div name="locate_box_well" id="locate_box_well" style="overflow-y: hidden; height:auto; width:auto; word-wrap: break-word;"> - </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>