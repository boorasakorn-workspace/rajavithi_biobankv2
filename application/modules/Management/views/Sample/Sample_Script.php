<script type="text/javascript">
	function removeDuplicateRows($table,callback){
	    function getVisibleRowText($row){
	        return $row.find('td:visible').not('td:first').text().toLowerCase();
	    }

	    $table.find('tr').each(function(index, row){
	        var $row = $(row);
	        $row.nextAll('tr').each(function(index, next){
	            var $next = $(next);
	            if(getVisibleRowText($next) == getVisibleRowText($row))
	                $next.remove();
	        })
	    });
	    if(callback){
	    	callback;
	    }
	}

	function generate(callback){
		var max = new Date().getFullYear() + 543;
		    min = max - 10;
		    select = document.getElementById('year_dropdown');

		for (var i = min; i<=max; i++){
		    var opt = document.createElement('option');
		    opt.value = i;
		    opt.innerHTML = i;
		    select.appendChild(opt);
		}
		select.value = "";
		callback;

	}
	$(document).ready(function(){
		if( $('#year_dropdown').length){
			
			generate(function(){
			});
		}

		$("#year_dropdown").change(function () {
			var fullyear = $(this).val();
			var year = fullyear.substring(2,4);
			window.location.href = '<?=base_url('Management/Sample/');?>' + year;
		});
		removeDuplicateRows($('#SampleTable'));
		var UserTable = $('#SampleTable').DataTable({
				ordering: false,
				scrollY: true,
				scrollX: true,
				paging: true,
				pageLength: 10,
				dom: 'frtp',
			});

		$('#management_content').on('resize', function () {
			UserTable.draw();
		});
		
		$(document).on('click','[newln-sample]',function(){
			$('#search_ln-modal').modal('show');	
		})

		$(document).on('click','#search_ln_submit',function(){
			var SampleID = $('#search_ln').val();
			$.get( "<?=base_url('Service/JSON_AnuData/Sample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);

				if(data['function']){
					switch(data['function']){
						case 'refresh':
							location.reload();
							break;
						case 'redirect':
							window.location.href = '<?=base_url();?>' + data['url'];
							break;
						case 'alert':
						console.log("TEST");
							swal({
			                    title: data['modal-data']['title'],
			                    text: data['modal-data']['text'],
			                    type: data['modal-data']['type'],
			                });
			                break;
					}
				}
				
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length ){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#method').val('add');
				$('#sample_edit_submit').text('Add');
				$('#edit_sample-modal .modal-title').text('Add Sample');
				$('#edit_sample-modal').modal('show');
			});
		});

		$(document).on('click','[new-sample]',function(){
			$('#method').val('add');
			$('#sample_edit_submit').text('Add');
			$('#edit_sample-modal .modal-title').text('Add Sample');
			$('#edit_sample-modal').modal('show');		
		});

		$(document).on('click','[add-sample]',function(){
			var SampleID = $(this).attr('add-sample');
			$.get( "<?=base_url('Service/JSON_AnuData/Sample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length ){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#method').val('add');
				$('#sample_edit_submit').text('Add');
				$('#edit_sample-modal .modal-title').text('Add Sample');
				$('#edit_sample-modal').modal('show');
			});			
		});

		$(document).on('click','[info-sample]',function(){
			var SampleID = $(this).attr('info-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#info_'+key).length ){
						$('#info_'+key).val(data['result'][key]);					
					}					
				});
				$('#info_sample-modal').modal('show');
			});
		});

		$(document).on('click','[edit-sample]',function(){
			var SampleID = $(this).attr('edit-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				console.log(data);				
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length ){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#method').val('edit');
				$('#sample_edit_submit').text('Edit');
				$('#edit_sample-modal .modal-title').text('Edit Sample');
				$('#edit_sample-modal').modal('show');
			});
		});

		$(document).on('click','[del-sample]',function(){
			var SampleID = $(this).attr('del-sample');
			$.get( "<?=base_url('Service/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				var targetFullname = data['result']['prefix'] + ' ' + data['result']['firstname'] + ' ' + data['result']['lastname'];
				$('#span_fullname').text(targetFullname);
				$('#del_sample_sid').val(data['result']['scanln']);
				$('#del_sample-modal').modal('show');
			});
		});

	})
</script>