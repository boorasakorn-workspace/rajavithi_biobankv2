<?php 
	//echo '<pre>'.var_export($Data['AnuSample']->result(),TRUE).'</pre>';
	//die();
?>

<div class="card rounded-0" id="register-form">
    <div class="card-header">
    	<div class="row">
			<div class="col"><h3>Database</h3></div>
			<!--
			<div class="col-4 text-right">				
				<select id="year_dropdown" name="year_dropdown" class="form-control form-control-lg rounded-0">
					<option value="">Select Year</option>
				</select>
			</div>
			-->
			<div class="col text-right">
				<button type="button" class="btn btn-primary mb-1" newln-sample> New Sample With LN </button>
				<button type="button" class="btn btn-success mb-1" new-sample> New Sample </button>
			</div>
		</div>
	</div>
</div>
<div class="card-body">	
	<div class="container-fluid">
		<table id="SampleTable" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
				    <th style="white-space: nowrap;">#</th>
				    <th style="white-space: nowrap;">Barcode</th>  
				    <th style="white-space: nowrap;">HN</th>  
                    <th style="white-space: nowrap;">Fullname</th>
                    <th style="white-space: nowrap;">Birthdate</th>
				    <th style="white-space: nowrap;">Age</th>
				    <th style="white-space: nowrap;">Gender</th>
                    <th style="white-space: nowrap;">Type</th>
                    <th style="white-space: nowrap;">DataStatus</th>
                    <th style="white-space: nowrap;">TubeStatus</th>
				    <th style="white-space: nowrap;">Manage</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($Data['AnuSample']->result() as $key => $Sample): ?>
					<tr>
					    <td style="white-space: nowrap;"><?=$key;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_SID;?></td>  
	                    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_HN;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_FULLNAME?></td>  
	                    <td style="white-space: nowrap;"><?=convert_date_format($Sample->SAMPLE_HBD);?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_AGE;?></td>
					    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_GENDER;?></td>
	                    <td style="white-space: nowrap;"><?=$Sample->SAMPLE_TYPE;?></td>
	                    <td style="white-space: nowrap;">
					    	<?php if( $Sample->DATA_STATUS == 1 ){ ?>
								<button type="button" class="btn btn-sm btn-success" info-sample="<?=$Sample->SAMPLE_SID;?>">Info</button>
						    <?php }else{ 
						    	echo "Not Exists";
						    } ?>	                    		
	                    </td>
	                    <td style="white-space: nowrap;"><?=($Sample->BOXADD_STATUS == 1?'In Box':'No Input');?></td>
					    <td style="white-space: nowrap;">
					    	<?php if( $Sample->DATA_STATUS == 1 ){ ?>
								<button type="button" class="btn btn-sm btn-warning" edit-sample="<?=$Sample->SAMPLE_SID;?>">Edit</button>
								<button type="button" class="btn btn-sm btn-danger" del-sample="<?=$Sample->SAMPLE_SID;?>">Delete</button>
						    <?php }else{ ?>
								<button type="button" class="btn btn-sm btn-success" add-sample="<?=$Sample->SAMPLE_SID;?>">Add</button>
						    <?php } ?>
					    </td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>