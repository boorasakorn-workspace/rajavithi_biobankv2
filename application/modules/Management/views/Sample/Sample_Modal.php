<div class="modal fade" id="search_ln-modal" tabindex="-1" role="dialog" aria-labelledby="search_ln-modal" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Search LN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('search_ln','','Search LN');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="search_ln_submit" data-dismiss="modal">Search</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="info_sample-modal" tabindex="-1" role="dialog" aria-labelledby="info_sample-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Sample Info</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-5">
                        <?=form_group_input('info_scanln','','SCAN LN');?>
                    </div>  
                    <div class="col">
                        <div class="form-group">
                            <?php
                                echo form_label(ucfirst('In Date'), 'info_indate');
                                echo datepicker_th('info_indate');
                            ?>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <?=form_group_input('info_intime','','In Time');?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <?=form_group_input('info_prefix','','Prefix');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_firstname','','Firstname');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_lastname','','Lastname');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?=form_group_input('info_hn','','HN');?>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <?php
                                echo form_label(ucfirst('birthdate'), 'info_birthdate');
                                echo datepicker_th('info_birthdate');
                            ?>
                        </div>
                    </div>
                    <div class="col-2">
                        <?=form_group_input('info_age','','Age');?>
                    </div>
                    <div class="col-2">
                        <div class="form-group">
                            <?=form_group_input('info_gender','','Gender');?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <?=form_group_input('info_sample_type','','Type Of Specimen');?>
                        </div>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_pjowner','','Project Owner');?>
                    </div>
                    <div class="col">
                        <?=form_group_input('info_consent','','Consent From');?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?=form_group_input('info_pjname','','Project Name');?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit_sample-modal" tabindex="-1" role="dialog" aria-labelledby="edit_sample-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Edit Sample</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/EditSample', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                    ?>
                    <?=form_input($method_input);?>
                    <div class="row">
                        <div class="col-5">
                            <?=form_group_input('scanln','','SCAN LN');?>
                        </div>  
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('In Date'), 'indate');
                                    echo datepicker_th('indate');
                                ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('In Time'), 'intime');
                                ?>
                                <input type="time" class="form-control" id="intime" name="intime" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <?php
                                echo form_label(ucfirst('prefix'), 'prefix');
                            ?>
                            <select name="prefix" id="prefix" class="form-control form-control-lg rounded-0">
                                <?php foreach($Data['SamplePrefix']->result() as $prefix_result) : ?>
                                    <option value="<?=$prefix_result->SAMPLE_PREFIX;?>"><?=$prefix_result->SAMPLE_PREFIX;?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col">
                            <?=form_group_input('firstname','');?>
                        </div>
                        <div class="col">
                            <?=form_group_input('lastname','');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('hn','');?>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('birthdate'), 'Birthdate');
                                    echo datepicker_th('birthdate');
                                ?>
                            </div>
                        </div>
                        <div class="col-2">
                            <?=form_group_input('age','');?>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('gender'), 'gender');
                                    echo dropdown_gender('');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('Type Of Specimen'), 'sample_type');
                                ?>
                                <select class="form-control form-control-lg rounded-0" id="sample_type" name="sample_type">
                                    <option value="">Please select</option>
                                    <?php foreach($Data['SampleType']->result() as $type_result) : ?>
                                        <option value="<?=$type_result->SAMPLE_TYPE;?>"><?=$type_result->SAMPLE_TYPE;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <?=form_group_input('pjowner','','Project Owner');?>
                        </div>
                        <div class="col">
                            <?=form_group_input('consent','','Consent From');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('pjname','','Project Name');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="sample_edit_submit" data-dismiss="modal" form_submit>Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>


<div class="modal fade" id="del_sample-modal" tabindex="-1" role="dialog" aria-labelledby="del_sample-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete Sample</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/DeleteSample', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'sample_sid',
                            'id' => 'del_sample_sid',
                        );
                    ?>
                    <?=form_input($hidden_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_fullname"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>