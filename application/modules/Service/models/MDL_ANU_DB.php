<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_ANU_DB extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function SamplePrefix(){
        $query_string = "SELECT  DISTINCT(TR_SAMPLE.SAMPLE_PREFIX) ".
                        "FROM    ".SCMPREFIX."TR_SAMPLE ".
                        "UNION SELECT pname.name as SAMPLE_PREFIX ".
                        "FROM anu.pname ";
        $query = $this->db->query($query_string);
        return $query;
    }

    public function SampleType(){
        $query_string = "SELECT  DISTINCT(TR_SAMPLE.SAMPLE_TYPE) ".
                        "FROM    ".SCMPREFIX."TR_SAMPLE ".
                        "UNION SELECT labspcm.name as SAMPLE_TYPE ".
                        "FROM anu.labspcm ";
        $query = $this->db->query($query_string);
        return $query;
    }

    public function SampleANU_LN($LN){
        $ANUDB = PARENTDB;
        $checkExists = "
            CAST((
                SELECT DISTINCT(1) FROM ".SCMPREFIX."TR_SAMPLE WHERE TR_SAMPLE.SAMPLE_SID = ".$ANUDB.".LN
            )AS VARCHAR(100)) AS DATA_STATUS
        ";
        $checkBoxAdd = "
            (
                SELECT DISTINCT(1) FROM ".SCMPREFIX."TR_BOX_ADD WHERE TR_BOX_ADD.SAMPLE_SID = ".$ANUDB.".LN AND TR_BOX_ADD.BOXADD_STATUS != 1
            ) AS BOXADD_STATUS
        ";
        $this->db->select("CAST(".$ANUDB.".LN AS VARCHAR(100)) as SAMPLE_SID",false);
        $this->db->select("".$ANUDB.".PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("".$ANUDB.".FNAME as SAMPLE_FNAME ",false);
        $this->db->select("".$ANUDB.".LNAME as SAMPLE_LNAME ",false);
        $this->db->select("".$ANUDB.".PNAME || ' ' || ".$ANUDB.".FNAME || ' ' || ".$ANUDB.".LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("CAST(".$ANUDB.".HN AS VARCHAR(100)) as SAMPLE_HN",false);
        $this->db->select("".$ANUDB.".LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("TO_DATE(".$ANUDB.".BRTHDATE,'DD/MM/YYYY') as SAMPLE_HBD",false);
        $this->db->select("TO_NUMBER(".$ANUDB.".AGE) as SAMPLE_AGE",false);
        $this->db->select("".$ANUDB.".GENDER as SAMPLE_GENDER",false);
        $this->db->select("".$ANUDB.".LVSTDATE as SAMPLE_DATE",false);
        $this->db->select("".$ANUDB.".LVSTTIME as SAMPLE_TIME",false);
        $this->db->select($checkExists,false);
        $this->db->select($checkBoxAdd,false);
        $this->db->from('ANU.'.$ANUDB.'');
        $this->db->where(''.$ANUDB.'.LN',"'".$LN."'",false);
        $query = $this->db->get();
        return $query;
    }

    public function SampleANU($LN = NULL,$Year = NULL){
        $ANUDB = PARENTDB;
        $checkExists = "
            CAST((
                SELECT DISTINCT(1) FROM ".SCMPREFIX."TR_SAMPLE WHERE TR_SAMPLE.SAMPLE_SID = ".$ANUDB.".LN
            )AS VARCHAR(100)) AS DATA_STATUS
        ";
        $checkBoxAdd = "
            (
                SELECT DISTINCT (1) FROM ".SCMPREFIX."TR_BOX_ADD WHERE TR_BOX_ADD.SAMPLE_SID = ".$ANUDB.".LN AND TR_BOX_ADD.BOXADD_STATUS != 1
            ) AS BOXADD_STATUS
        ";
        $this->db->select("CAST(".$ANUDB.".LN AS VARCHAR(100)) as SAMPLE_SID",false);
        $this->db->select("".$ANUDB.".PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("".$ANUDB.".FNAME as SAMPLE_FNAME ",false);
        $this->db->select("".$ANUDB.".LNAME as SAMPLE_LNAME ",false);
        $this->db->select("".$ANUDB.".PNAME || ' ' || ".$ANUDB.".FNAME || ' ' || ".$ANUDB.".LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("CAST(".$ANUDB.".HN AS VARCHAR(100)) as SAMPLE_HN",false);
        $this->db->select("".$ANUDB.".LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("TO_DATE(".$ANUDB.".BRTHDATE,'DD/MM/YYYY') as SAMPLE_HBD",false);
        $this->db->select("TO_NUMBER(".$ANUDB.".AGE) as SAMPLE_AGE",false);
        $this->db->select("".$ANUDB.".GENDER as SAMPLE_GENDER",false);
        $this->db->select($checkExists,false);
        $this->db->select($checkBoxAdd,false);
        $this->db->from('ANU.'.$ANUDB.'');
        $this->db->join('ANU.TR_BOX_ADD',''.$ANUDB.'.LN = TR_BOX_ADD.SAMPLE_SID AND TR_BOX_ADD.BOXADD_STATUS != 1','RIGHT',false);
        $this->db->where("".$ANUDB.".LN LIKE '__41%' ",null,false);      
        $query1= $this->db->get_compiled_select();
        $this->db->reset_query();

        $checkExists2 = " '1' AS DATA_STATUS    ";
        $checkBoxAdd2 = "
            (
                SELECT DISTINCT (1) FROM ".SCMPREFIX."TR_BOX_ADD WHERE TR_BOX_ADD.SAMPLE_SID = TR_SAMPLE.SAMPLE_SID AND TR_BOX_ADD.BOXADD_STATUS != 1
            ) AS BOXADD_STATUS
        ";
        $this->db->select('CAST(TR_SAMPLE.SAMPLE_SID AS VARCHAR(100)) AS SAMPLE_SID, TR_SAMPLE.SAMPLE_PREFIX, TR_SAMPLE.SAMPLE_FNAME, TR_SAMPLE.SAMPLE_LNAME',FALSE);
        $this->db->select("TR_SAMPLE.SAMPLE_PREFIX || ' ' || TR_SAMPLE.SAMPLE_FNAME || ' ' || TR_SAMPLE.SAMPLE_LNAME as SAMPLE_FULLNAME",FALSE);
        $this->db->select("TR_SAMPLE.SAMPLE_HN as SAMPLE_HN",false);
        $this->db->select("TR_SAMPLE.SAMPLE_TYPE as SAMPLE_TYPE",false);
        $this->db->select("TR_SAMPLE.SAMPLE_HBD as SAMPLE_HBD",false);
        $this->db->select("TR_SAMPLE.SAMPLE_AGE as SAMPLE_AGE",false);
        $this->db->select("TR_SAMPLE.SAMPLE_GENDER as SAMPLE_GENDER",false);
        $this->db->select($checkExists2,false);
        $this->db->select($checkBoxAdd2,false);
        $this->db->from('ANU.TR_SAMPLE');
        if( isset($LN) && $LN != NULL ){
            $this->db->where('TR_SAMPLE.SAMPLE_SID',"'".$LN."'",false);           
        }
        $query2= $this->db->get_compiled_select();
        $this->db->reset_query();
        $query = $this->db->query("$query1 UNION $query2");

        return $query;
    }

    public function SampleBoxAdd($LN = NULL){
        $ANUDB = PARENTDB;
        $checkExists = "
            (
                SELECT DISTINCT('1') FROM ".SCMPREFIX."tr_sample WHERE tr_sample.sample_sid = ".$ANUDB.".LN
            ) AS DATA_STATUS
        ";
        $this->db->select("CAST(TR_BOX_ADD.SAMPLE_SID AS VARCHAR(100)) as SAMPLE_SID",false);
        $this->db->select("".$ANUDB.".PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("".$ANUDB.".FNAME as SAMPLE_FNAME ",false);
        $this->db->select("".$ANUDB.".LNAME as SAMPLE_LNAME ",false);
        $this->db->select("".$ANUDB.".PNAME || ' ' || ".$ANUDB.".FNAME || ' ' || ".$ANUDB.".LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("CAST(".$ANUDB.".HN AS VARCHAR(100)) as SAMPLE_HN",false);
        $this->db->select("".$ANUDB.".LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("TO_DATE(".$ANUDB.".BRTHDATE,'DD/MM/YYYY') as SAMPLE_HBD",false);
        $this->db->select("TO_NUMBER(".$ANUDB.".AGE) as SAMPLE_AGE",false);
        $this->db->select("".$ANUDB.".GENDER as SAMPLE_GENDER",false);
        $this->db->select($checkExists,false);
        $this->db->from('ANU.'.$ANUDB.'');
        $this->db->join('ANU.TR_BOX_ADD',''.$ANUDB.'.LN = TR_BOX_ADD.SAMPLE_SID AND TR_BOX_ADD.BOXADD_STATUS != 1','right',false); 
        $query1= $this->db->get_compiled_select();
        $this->db->reset_query();

        $checkExists2 = " '1' AS DATA_STATUS    ";
        $this->db->select('TR_SAMPLE.SAMPLE_SID, TR_SAMPLE.SAMPLE_PREFIX, TR_SAMPLE.SAMPLE_FNAME, TR_SAMPLE.SAMPLE_LNAME',FALSE);
        $this->db->select("TR_SAMPLE.SAMPLE_PREFIX || ' ' || TR_SAMPLE.SAMPLE_FNAME || ' ' || TR_SAMPLE.SAMPLE_LNAME as SAMPLE_FULLNAME",FALSE);
        $this->db->select("TR_SAMPLE.SAMPLE_HN as SAMPLE_HN",false);
        $this->db->select("TR_SAMPLE.SAMPLE_TYPE as SAMPLE_TYPE",false);
        $this->db->select("TR_SAMPLE.SAMPLE_HBD as SAMPLE_HBD",false);
        $this->db->select("TR_SAMPLE.SAMPLE_AGE as SAMPLE_AGE",false);
        $this->db->select("TR_SAMPLE.SAMPLE_GENDER as SAMPLE_GENDER",false);
        $this->db->select($checkExists2,false);
        $this->db->from('ANU.TR_SAMPLE');
        $this->db->join('ANU.TR_BOX_ADD','TR_SAMPLE.SAMPLE_SID = TR_BOX_ADD.SAMPLE_SID AND TR_BOX_ADD.BOXADD_STATUS != 1','right',false);   
        if( isset($LN) && $LN != NULL ){
            $this->db->where('TR_SAMPLE.SAMPLE_SID',"'".$LN."'",false);           
        }else{
            $this->db->where('TR_BOX_ADD.SAMPLE_SID !='," NULL",false);
        }
        $query2= $this->db->get_compiled_select();
        $this->db->reset_query();
        $query = $this->db->query("$query1 UNION $query2");

        return $query;
    }

    function GetSample_ANU($LN){
        $ANUDB = PARENTDB;
        $this->db->select("".$ANUDB.".LN as SAMPLE_SID",false);
        $this->db->select("".$ANUDB.".PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("".$ANUDB.".FNAME as SAMPLE_FNAME ",false);
        $this->db->select("".$ANUDB.".LNAME as SAMPLE_LNAME ",false);
        $this->db->select("".$ANUDB.".PNAME || ' ' || ".$ANUDB.".FNAME || ' ' || ".$ANUDB.".LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("".$ANUDB.".HN as SAMPLE_HN",false);
        $this->db->select("".$ANUDB.".LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("".$ANUDB.".BRTHDATE as SAMPLE_HBD",false);
        $this->db->select("".$ANUDB.".AGE as SAMPLE_AGE",false);
        $this->db->select("".$ANUDB.".GENDER as SAMPLE_GENDER",false);
        $this->db->select("".$ANUDB.".LVSTDATE as SAMPLE_DATE",false);
        $this->db->select("".$ANUDB.".LVSTTIME as SAMPLE_TIME",false);
        $this->db->from('ANU.'.$ANUDB.'');
        $this->db->where(''.$ANUDB.'.LN',"'".$LN."'",false);
        $query = $this->db->get();
        $Data = array(
            'scanln' => $query->row()->SAMPLE_SID,
            'indate' => convert_date_format($query->row()->SAMPLE_DATE),
            'intime' => date("H:i", $query->row()->SAMPLE_TIME),
            'prefix' => $query->row()->SAMPLE_PREFIX,
            'firstname' => $query->row()->SAMPLE_FNAME,
            'lastname' => $query->row()->SAMPLE_LNAME,
            'hn' => $query->row()->SAMPLE_HN,
            'birthdate' => convert_date_format($query->row()->SAMPLE_HBD),
            'age' => $query->row()->SAMPLE_AGE,
            'gender' => $query->row()->SAMPLE_GENDER,
            'sample_type' => $query->row()->SAMPLE_TYPE,
        );
        
        return $Data;
    }

}