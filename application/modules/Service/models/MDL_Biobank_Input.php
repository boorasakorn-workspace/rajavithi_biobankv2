<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_Biobank_Input extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    // Freezer Data
    public function ListFreezer(){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'MS_FREEZER');
        $this->db->where('FREEZER_STATUS !=','1',false);
        $this->db->order_by('FREEZER_NAME','ASC');
        $query = $this->db->get();      
        return $query;
    }

    // Freezer Floor
    public function ListFreezerFloor($ID = NULL){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'MS_FREEZER_FLOOR');
        $this->db->where('FREEZER_FLOOR_STATUS !=','1',false);
        if($ID != NULL){ $this->db->where('FREEZER_ID',$ID); }
        $this->db->order_by('FREEZER_FLOOR_EDIT','ASC');
        $query = $this->db->get();      
        return $query;
    }

    // Rack
    public function ListRack($ID = NULL){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'MS_RACK');
        $this->db->where('RACK_STATUS !=','1',false);
        if($ID != NULL){ $this->db->where('FREEZER_FLOOR_ID',$ID); }
        $this->db->order_by('RACK_EDIT','ASC');
        $query = $this->db->get();      
        return $query;
    }

    // Rack Floor
    public function ListRackFloor($ID = NULL){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'MS_RACK_FLOOR');
        $this->db->where('RACK_FLOOR_STATUS !=','1',false);
        if($ID != NULL){ $this->db->where('RACK_ID',$ID); }
        $this->db->order_by('RACK_FLOOR_EDIT','ASC');
        $query = $this->db->get();      
        return $query;
    }

    // List of All Box in Freezer
    public function ListBox($ID = NULL,$Locate = NULL){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'TR_FREEZER_ADD');
        $this->db->join(SCMPREFIX.'MS_BOX', "MS_BOX.BOX_BARCODE = TR_FREEZER_ADD.BOX_BARCODE AND MS_BOX.BOX_STATUS != '1'", 'LEFT');
        $this->db->where('FREEZERADD_STATUS','0');
        if($ID != NULL && $Locate != NULL){ 
            switch ($Locate) {
                case 'Freezer':
                    $this->db->where('FREEZER_ID',$ID);
                    break;
                case 'FreezerFloor':
                    $this->db->where('FREEZER_FLOOR_ID',$ID);
                    break;
                case 'Rack':
                    $this->db->where('RACK_ID',$ID);
                    break;
                case 'RackFloor':
                    $this->db->where('RACK_FLOOR_ID',$ID);
                    break;
            }
        }
        $this->db->order_by('MS_BOX.BOX_NAME','ASC');
        $query = $this->db->get();      
        return $query;
    }

    // Box Data
    public function BoxData($Barcode = NULL){
        $CountTube = "
            (
                SELECT COUNT(TR_BOX_ADD.BOX_ID) AS COUNT_TUBE
                FROM ".SCMPREFIX."TR_BOX_ADD
                WHERE TR_BOX_ADD.BOX_ID = MS_BOX.BOX_ID
            ) AS COUNT_TUBE";
        $this->db->select('MS_BOX.*');
        $this->db->select($CountTube);
        $this->db->from(SCMPREFIX.'MS_BOX');
        $this->db->where('BOX_STATUS !=','1',FALSE);
        if($Barcode != NULL){ $this->db->where('BOX_BARCODE',$Barcode); }
        $this->db->order_by('MS_BOX.BOX_NAME','ASC');
        $query = $this->db->get();
        return ($Barcode != NULL ? $query->row() : $query->result() );
    }

    // BoxAdd Tube Data
    public function BoxAddData($Barcode = NULL){
        if($Barcode != NULL){ 
            $BOX_ID = $this->BoxData($Barcode)->BOX_ID;
        }
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'TR_BOX_ADD');        
        if($Barcode != NULL){ $this->db->where('BOX_ID',$BOX_ID); }
        $this->db->where('BOXADD_STATUS !=','1',FALSE);
        $this->db->order_by('TR_BOX_ADD.BOX_ID','ASC');
        $query = $this->db->get();
        return $query;
    }

    // Box Locate
    public function BoxLocate($BoxBarcode = NULL){
        $query_location = "
            (
                SELECT MS_FREEZER.FREEZER_NAME 
                FROM ".SCMPREFIX."MS_FREEZER 
                WHERE MS_FREEZER.FREEZER_ID = TR_FREEZER_ADD.FREEZER_ID
            ) AS FREEZER_NAME,
            (
                SELECT MS_FREEZER_FLOOR.FREEZER_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_FREEZER_FLOOR 
                WHERE MS_FREEZER_FLOOR.FREEZER_FLOOR_ID = TR_FREEZER_ADD.FREEZER_FLOOR_ID
            ) AS FREEZER_FLOOR_NAME,
            (
                SELECT MS_RACK.RACK_NAME 
                FROM ".SCMPREFIX."MS_RACK 
                WHERE MS_RACK.RACK_ID = TR_FREEZER_ADD.RACK_ID
            ) AS RACK_NAME,
            (
                SELECT MS_RACK_FLOOR.RACK_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_RACK_FLOOR 
                WHERE MS_RACK_FLOOR.RACK_FLOOR_ID = TR_FREEZER_ADD.RACK_FLOOR_ID
            ) AS RACK_FLOOR_NAME
        ";
        $this->db->select('FREEZER_ID,FREEZER_FLOOR_ID,RACK_ID,RACK_FLOOR_ID,MS_BOX.BOX_ID,
            MS_BOX.BOX_NAME,MS_BOX.BOX_BARCODE',FALSE);
        $this->db->select($query_location,FALSE);
        $this->db->from(SCMPREFIX.'MS_BOX');
        $this->db->join(SCMPREFIX.'TR_FREEZER_ADD', 'TR_FREEZER_ADD.BOX_BARCODE = MS_BOX.BOX_BARCODE', 'LEFT',false);
        if(isset($BoxBarcode) && $BoxBarcode != NULL){
            $this->db->where('MS_BOX.BOX_BARCODE =',"'".$BoxBarcode."'",FALSE);
        }       
        $query = $this->db->get();
        if($BoxBarcode != NULL && $query->num_rows() > 0){
            $Data = array(
                'id_freezer' => $query->row()->FREEZER_ID,
                'id_freezer_floor' => $query->row()->FREEZER_FLOOR_ID,
                'id_rack' => $query->row()->RACK_ID,
                'id_rack_floor' => $query->row()->RACK_FLOOR_ID,
                'id_box' => $query->row()->BOX_ID,
                'barcode_box' => $query->row()->BOX_BARCODE,
                'locate_freezer' => $query->row()->FREEZER_NAME,
                'locate_freezer_floor' => $query->row()->FREEZER_FLOOR_NAME,
                'locate_rack' => $query->row()->RACK_NAME,
                'locate_rack_floor' => $query->row()->RACK_FLOOR_NAME,
                'locate_box' => $query->row()->BOX_NAME,
            );
        }else if($query->num_rows() > 0){
            $Data = $query->result();
        }else{
            $Data = array();
        }
        return $Data;
    }

    // Tube Locate
    public function TubeLocate($LN){
        $ANUDB = PARENTDB;
        $query_location = "
            (
                SELECT MS_FREEZER.FREEZER_NAME 
                FROM ".SCMPREFIX."MS_FREEZER 
                WHERE MS_FREEZER.FREEZER_ID = TR_FREEZER_ADD.FREEZER_ID
            ) AS FREEZER_NAME,
            (
                SELECT MS_FREEZER_FLOOR.FREEZER_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_FREEZER_FLOOR 
                WHERE MS_FREEZER_FLOOR.FREEZER_FLOOR_ID = TR_FREEZER_ADD.FREEZER_FLOOR_ID
            ) AS FREEZER_FLOOR_NAME,
            (
                SELECT MS_RACK.RACK_NAME 
                FROM ".SCMPREFIX."MS_RACK 
                WHERE MS_RACK.RACK_ID = TR_FREEZER_ADD.RACK_ID
            ) AS RACK_NAME,
            (
                SELECT MS_RACK_FLOOR.RACK_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_RACK_FLOOR 
                WHERE MS_RACK_FLOOR.RACK_FLOOR_ID = TR_FREEZER_ADD.RACK_FLOOR_ID
            ) AS RACK_FLOOR_NAME,
            (
                SELECT TR_SAMPLE.SAMPLE_PREFIX || ' ' || TR_SAMPLE.SAMPLE_FNAME || ' ' || TR_SAMPLE.SAMPLE_LNAME
                FROM ".SCMPREFIX."TR_SAMPLE
                WHERE TR_SAMPLE.SAMPLE_SID = TR_BOX_ADD.SAMPLE_SID
            ) AS SAMPLE_NAME
        ";
        $this->db->select('FREEZER_ID,FREEZER_FLOOR_ID,RACK_ID,RACK_FLOOR_ID,MS_BOX.BOX_ID,
            MS_BOX.BOX_NAME,MS_BOX.BOX_BARCODE,TR_BOX_ADD.BOXADD_WELL',FALSE);
        $this->db->select("TR_BOX_ADD.SAMPLE_SID as SAMPLE_SID",false);
        $this->db->select("".$ANUDB.".PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("".$ANUDB.".FNAME as SAMPLE_FNAME ",false);
        $this->db->select("".$ANUDB.".LNAME as SAMPLE_LNAME ",false);
        $this->db->select("".$ANUDB.".PNAME || ' ' || ".$ANUDB.".FNAME || ' ' || ".$ANUDB.".LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("".$ANUDB.".HN as SAMPLE_HN",false);
        $this->db->select("".$ANUDB.".LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("".$ANUDB.".BRTHDATE as SAMPLE_HBD",false);
        $this->db->select("".$ANUDB.".AGE as SAMPLE_AGE",false);
        $this->db->select("".$ANUDB.".GENDER as SAMPLE_GENDER",false);
        $this->db->select($query_location,FALSE);
        $this->db->from(SCMPREFIX.'MS_BOX');
        $this->db->join(SCMPREFIX.'TR_BOX_ADD', 'TR_BOX_ADD.BOX_ID = MS_BOX.BOX_ID', 'RIGHT',false);
        $this->db->join(SCMPREFIX.'TR_FREEZER_ADD', 'TR_FREEZER_ADD.BOX_BARCODE = MS_BOX.BOX_BARCODE', 'LEFT',false);
        $this->db->where('TR_BOX_ADD.SAMPLE_SID =',"'".$LN."'",FALSE);
        $this->db->join(SCMPREFIX.''.$ANUDB.'', ''.$ANUDB.'.LN = TR_BOX_ADD.SAMPLE_SID', 'LEFT',false);
        $this->db->where('TR_BOX_ADD.SAMPLE_SID =',"'".$LN."'",FALSE);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $Data = array(
                'info_sample_name' => $query->row()->SAMPLE_NAME,
                'info_sample_sid' => $LN,
                'id_freezer' => $query->row()->FREEZER_ID,
                'id_freezer_floor' => $query->row()->FREEZER_FLOOR_ID,
                'id_rack' => $query->row()->RACK_ID,
                'id_rack_floor' => $query->row()->RACK_FLOOR_ID,
                'id_box' => $query->row()->BOX_ID,
                'barcode_box' => $query->row()->BOX_BARCODE,
                'locate_freezer' => $query->row()->FREEZER_NAME,
                'locate_freezer_floor' => $query->row()->FREEZER_FLOOR_NAME,
                'locate_rack' => $query->row()->RACK_NAME,
                'locate_rack_floor' => $query->row()->RACK_FLOOR_NAME,
                'locate_box' => $query->row()->BOX_NAME,
                'locate_box_well' => $query->row()->BOXADD_WELL,
            );
        }else{
            $Data = array();
        }
        return $Data;
    }
    
    // Tube Locate
    public function FullTubeLocate($FreezerID = NULL){
        $ANUDB = PARENTDB;
        $query_location = "
            (
                SELECT MS_FREEZER.FREEZER_NAME 
                FROM ".SCMPREFIX."MS_FREEZER 
                WHERE MS_FREEZER.FREEZER_ID = TR_FREEZER_ADD.FREEZER_ID
            ) AS FREEZER_NAME,
            (
                SELECT MS_FREEZER_FLOOR.FREEZER_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_FREEZER_FLOOR 
                WHERE MS_FREEZER_FLOOR.FREEZER_FLOOR_ID = TR_FREEZER_ADD.FREEZER_FLOOR_ID
            ) AS FREEZER_FLOOR_NAME,
            (
                SELECT MS_RACK.RACK_NAME 
                FROM ".SCMPREFIX."MS_RACK 
                WHERE MS_RACK.RACK_ID = TR_FREEZER_ADD.RACK_ID
            ) AS RACK_NAME,
            (
                SELECT MS_RACK_FLOOR.RACK_FLOOR_NAME 
                FROM ".SCMPREFIX."MS_RACK_FLOOR 
                WHERE MS_RACK_FLOOR.RACK_FLOOR_ID = TR_FREEZER_ADD.RACK_FLOOR_ID
            ) AS RACK_FLOOR_NAME,
            (
                SELECT TR_SAMPLE.SAMPLE_PREFIX || ' ' || TR_SAMPLE.SAMPLE_FNAME || ' ' || TR_SAMPLE.SAMPLE_LNAME
                FROM ".SCMPREFIX."TR_SAMPLE
                WHERE TR_SAMPLE.SAMPLE_SID = TR_BOX_ADD.SAMPLE_SID
            ) AS SAMPLE_NAME
        ";
        $this->db->select('FREEZER_ID,FREEZER_FLOOR_ID,RACK_ID,RACK_FLOOR_ID,MS_BOX.BOX_ID,
            MS_BOX.BOX_NAME,MS_BOX.BOX_BARCODE,TR_BOX_ADD.BOXADD_WELL',FALSE);         
        $this->db->select("TR_BOX_ADD.SAMPLE_SID as SAMPLE_SID",false);        
        $this->db->select("TR_SAMPLE.SAMPLE_PREFIX, TR_SAMPLE.SAMPLE_FNAME, TR_SAMPLE.SAMPLE_LNAME",false);
        $this->db->select("TR_SAMPLE.SAMPLE_PREFIX || ' ' || TR_SAMPLE.SAMPLE_FNAME || ' ' || TR_SAMPLE.SAMPLE_LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("TR_SAMPLE.SAMPLE_HN, TR_SAMPLE.SAMPLE_TYPE, TR_SAMPLE.SAMPLE_HBD, TR_SAMPLE.SAMPLE_AGE, TR_SAMPLE.SAMPLE_GENDER",false);
        /*
        $this->db->select("LAB_ANU.PNAME as SAMPLE_PREFIX ",false);
        $this->db->select("LAB_ANU.FNAME as SAMPLE_FNAME ",false);
        $this->db->select("LAB_ANU.LNAME as SAMPLE_LNAME ",false);
        $this->db->select("LAB_ANU.PNAME || ' ' || LAB_ANU.FNAME || ' ' || LAB_ANU.LNAME as SAMPLE_FULLNAME ",false);
        $this->db->select("LAB_ANU.HN as SAMPLE_HN",false);
        $this->db->select("LAB_ANU.LABSPCM as SAMPLE_TYPE",false);
        $this->db->select("LAB_ANU.BRTHDATE as SAMPLE_HBD",false);
        $this->db->select("LAB_ANU.AGE as SAMPLE_AGE",false);
        $this->db->select("LAB_ANU.GENDER as SAMPLE_GENDER",false);
        */
        $this->db->select($query_location,FALSE);
        $this->db->from(SCMPREFIX.'MS_BOX');
        $this->db->join(SCMPREFIX.'TR_BOX_ADD', 'TR_BOX_ADD.BOX_ID = MS_BOX.BOX_ID', 'RIGHT',false);
        $this->db->join(SCMPREFIX.'TR_FREEZER_ADD', 'TR_FREEZER_ADD.BOX_BARCODE = MS_BOX.BOX_BARCODE', 'LEFT',false);
        $this->db->join(SCMPREFIX.'TR_SAMPLE', 'TR_SAMPLE.SAMPLE_SID = TR_BOX_ADD.SAMPLE_SID', 'LEFT',false);
        $this->db->join(SCMPREFIX.''.$ANUDB.'', ''.$ANUDB.'.LN = TR_BOX_ADD.SAMPLE_SID', 'LEFT',false);
        if( isset($FreezerID) && $FreezerID != NULL){
            $this->db->where('TR_FREEZER_ADD.FREEZER_ID =',$FreezerID,FALSE);
        }       
        $this->db->where('TR_BOX_ADD.SAMPLE_SID !=',NULL,FALSE);
        $query = $this->db->get();
        return $query;
    }
    
    function GetSample($LN){
        $this->db->select("*",false);
        $this->db->from(SCMPREFIX.'TR_SAMPLE');
        $this->db->where('TR_SAMPLE.SAMPLE_SID',$LN);
        $query = $this->db->get();
        $Data = array(
            'scanln' => $query->row()->SAMPLE_SID,
            'indate' => convert_date_format($query->row()->SAMPLE_DATE),
            'intime' => date("H:i", $query->row()->SAMPLE_TIME),
            'prefix' => $query->row()->SAMPLE_PREFIX,
            'firstname' => $query->row()->SAMPLE_FNAME,
            'lastname' => $query->row()->SAMPLE_LNAME,
            'hn' => $query->row()->SAMPLE_HN,
            'birthdate' => convert_date_format($query->row()->SAMPLE_HBD),
            'age' => $query->row()->SAMPLE_AGE,
            'gender' => $query->row()->SAMPLE_GENDER,
            'sample_type' => $query->row()->SAMPLE_TYPE,
            'pjowner' => $query->row()->SAMPLE_PJOWNER,
            'consent' => $query->row()->SAMPLE_CONSENT,
            'pjname' => $query->row()->SAMPLE_PJNAME,
        );
        return $Data;
    }

    function checkBox($BoxBarcode){
        $this->db->select('MS_BOX.BOX_ID');
        $this->db->from(SCMPREFIX.'MS_BOX');
        $this->db->where('MS_BOX.BOX_BARCODE',$BoxBarcode,false);
        $this->db->where('MS_BOX.BOX_STATUS',0,false);
        $query = $this->db->get();
        return ( count($query->result()) > 0 ? true : false );
    }

    function checkAdd($BoxBarcode){
        $this->db->select('TR_FREEZER_ADD.FREEZERADD_ID');
        $this->db->from(SCMPREFIX.'TR_FREEZER_ADD');
        $this->db->where('TR_FREEZER_ADD.BOX_BARCODE',$BoxBarcode,false);
        $this->db->where('TR_FREEZER_ADD.FREEZERADD_STATUS',0,false);
        $query = $this->db->get();
        return ( count($query->result()) > 0 ? true : false );
    }
}