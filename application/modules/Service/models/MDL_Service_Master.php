<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_Service_Master extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function Insert($Table,$Data){
        $this->db->insert($Table,$Data,false);
        $this->Log('1',$Table);
    }

    function Update($Table,$Data,$Where){        
        foreach($Data as $key => $value){
            $this->db->set($key,$value,FALSE); 
        }        
        $this->db->where($Where,false);
        $this->db->update($Table);
        $this->Log('2',$Table);
    }

    function Delete($Table,$Data){
        $this->db->delete($Table, $Data);
        $this->Log('3',$Table);
    }

    function Log($Method,$Table){
        switch ($Table) {
            case 'MS_FREEZER':
                $CODE_TABLE = '11';
                break;
            case 'MS_FREEZER_FLOOR':
                $CODE_TABLE = '12';
                break;
            case 'MS_RACK':
                $CODE_TABLE = '13';
                break;
            case 'MS_RACK_FLOOR':
                $CODE_TABLE = '14';
                break;
            case 'MS_BOX':
                $CODE_TABLE = '15';
                break;
            case 'TR_SAMPLE':
                $CODE_TABLE = '21';
                break;
            case 'USER_ANU':
                $CODE_TABLE = '22';
                break;
            case 'TR_FREEZER_ADD':
                $CODE_TABLE = '23';
                break;
            case 'TR_BOX_ADD':
                $CODE_TABLE = '24';
                break;
            default:
                $CODE_TABLE = '0';
                break;
        }
        $LOGITEM_CODE = $CODE_TABLE . $Method;
        $InsertData = array(
            'LOG_ITEM' => intval($LOGITEM_CODE),
            'LOG_STATUS' => str_escape('1'),
            'LOG_USER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            'LOG_DATETIME' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
            'LOG_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            'LOG_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
            'LOG_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            'LOG_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
        );
        $this->db->insert(SCMPREFIX.'MS_LOG',$InsertData,false);        
    }
}