<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');
        $this->load->model('MDL_Service_Master');
    }

    public function AnuData($Select,$ID = NULL){
        $this->load->model('MDL_ANU_DB');
        switch ($Select) {
            case 'Sample':
                $Data = $this->MDL_ANU_DB->SampleANU($ID);
                break;
            case 'Prefix':
                $Data = $this->MDL_ANU_DB->SamplePrefix();
                break;
            case 'SampleType':
                $Data = $this->MDL_ANU_DB->SampleType();
                break;
        }
        return $Data;
    }

    public function JSON_AnuData($Select,$ID = NULL){
        $this->load->model('MDL_ANU_DB');
        switch ($Select) {
            case 'Sample':
                $ID = ( strlen($ID)>10 ? substr($ID,0,10) : $ID  ); //ตัด 2 ตัวท้ายออก (เลข specimen)
                $query = $this->MDL_ANU_DB->SampleANU_LN($ID);
                if($query->num_rows() > 0){
                    $Data = array(
                        'scanln' => $query->row()->SAMPLE_SID,
                        'indate' => convert_date_format($query->row()->SAMPLE_DATE),
                        'intime' => date("H:i", $query->row()->SAMPLE_TIME),
                        'prefix' => $query->row()->SAMPLE_PREFIX,
                        'firstname' => $query->row()->SAMPLE_FNAME,
                        'lastname' => $query->row()->SAMPLE_LNAME,
                        'hn' => $query->row()->SAMPLE_HN,
                        'birthdate' => convert_date_format($query->row()->SAMPLE_HBD),
                        'age' => $query->row()->SAMPLE_AGE,
                        'gender' => $query->row()->SAMPLE_GENDER,
                        'sample_type' => $query->row()->SAMPLE_TYPE,
                    );
                    $Message = array(
                        'status' => (bool)true,
                        'message' => "Success",
                        'result' => $Data,
                    );
                }else{
                    $Message = array(
                        'status' => (bool)false,
                        'message' => "No Data",
                        'function' => "alert",
                        'modal-data' => array(
                            'title' => 'Failed',
                            'text' => 'No Data',
                            'type' => 'error'
                        ),
                    );
                }
                break;
            case 'Prefix':
                $Data = $this->MDL_ANU_DB->SamplePrefix();
                break;
            case 'SampleType':
                $Data = $this->MDL_ANU_DB->SampleType();
                break;
        }
        echo json_encode($Message);
    }

    public function GetSample($LN){
        $this->load->model('MDL_Biobank_Input');
        $Query = $this->MDL_Biobank_Input->GetSample($LN);
        $Message = array(
            'status' => (bool)true,
            'message' => "Success",
            'result' => $Query,
        );
        echo json_encode($Message);
    }
    
    public function TubeLocate($LN){
        $this->load->model('MDL_Biobank_Input');
        $Query = $this->MDL_Biobank_Input->TubeLocate($LN);
        if( count($Query) > 0){
            $Message = array(
                'status' => (bool)true,
                'message' => "Success",
                'result' => $Query,
            );            
        }else{
            $Message = array(
                'status' => (bool)false,
                'message' => "No Location",
            );            
        }
        echo json_encode($Message);
    }

    public function FullTubeLocate($FreezerID){
        $this->load->model('MDL_Biobank_Input');
        $Data = $this->MDL_Biobank_Input->FullTubeLocate($FreezerID);
        return $Data;
    }

    public function ListAdd($Locate,$ID = NULL,$BoxLocate = NULL){
        $this->load->model('MDL_Biobank_Input');
        switch ($Locate) {
            case 'Freezer':
                $Data = $this->MDL_Biobank_Input->ListFreezer();
                break;
            case 'FreezerFloor':
                $Data = $this->MDL_Biobank_Input->ListFreezerFloor($ID);
                break;
            case 'Rack':
                $Data = $this->MDL_Biobank_Input->ListRack($ID);
                break;
            case 'RackFloor':
                $Data = $this->MDL_Biobank_Input->ListRackFloor($ID);
                break;
            case 'Box':
                $Data = $this->MDL_Biobank_Input->ListBox($ID,$BoxLocate);
                break;
        }
        return $Data;
    }

    public function ListData($Locate,$ID = NULL){
        $this->load->model('MDL_Biobank_Input');
        switch ($Locate) {
            case 'Box':
                $Data = $this->MDL_Biobank_Input->BoxData($ID);
                break;
        }
        return $Data;
    }

    public function JSON_ListData($Locate,$ID = NULL){
        $this->load->model('MDL_Biobank_Input');
        switch ($Locate) {
            case 'Box':
                $Data = $this->MDL_Biobank_Input->BoxData($ID);
                if( $ID != NULL && $Data != NULL){                    
                    $Data->BOX_DATE = convert_date_format($Data->BOX_DATE);
                    $Data->BOX_CWHEN = convert_date_format($Data->BOX_CWHEN);
                    $Data->BOX_MWHEN = convert_date_format($Data->BOX_MWHEN);
                    $Data->BOX_TIME = date("H:i", strtotime($Data->BOX_TIME));
                    $Message = array(
                        'status' => (bool)true,
                        'message' => "Success",
                        'result' => $Data,
                    );
                    echo json_encode($Message);
                }
                break;
        }
        return $Data;
    }

    public function DataAdd($Locate,$ID = NULL){
        $this->load->model('MDL_Biobank_Input');
        switch ($Locate) {
            case 'Box':
                $Data = $this->MDL_Biobank_Input->BoxAddData($ID);
                break;
        }
        return $Data;
    }

    public function DataLocate($Locate,$ID = NULL){
        $this->load->model('MDL_Biobank_Input');
        switch ($Locate) {
            case 'Box':
                $Data = $this->MDL_Biobank_Input->BoxLocate($ID);
                break;
        }
        return $Data;
    }

    public function Output(){
        
        $InputData = $this->input->post();
        $InputBarcode = $InputData['input_barcode'];
        switch($InputData['method']){
            case 'tube':
                $InputBarcode = ( strlen($InputBarcode)>10 ? substr($InputBarcode,0,10) : $InputBarcode  ); //ตัด 2 ตัวท้ายออก (เลข specimen)
                $TargetTB = SCMPREFIX.'TR_BOX_ADD';
                $TargetID = array(
                    'SAMPLE_SID' => $InputBarcode,
                );
            break;
            case 'box':
                $TargetTB = SCMPREFIX.'TR_FREEZER_ADD';
                $TargetID = array(
                    'FREEZER_ID' => intval($this->sessionextend->GetFreezer()),
                    'BOX_BARCODE' => $InputBarcode,
                );
            break;
        }
        $this->MDL_Service_Master->Delete($TargetTB,$TargetID);
        $Message = array(
            'status' => (bool)true,
            'message' => "Success",
        );
        echo json_encode($Message);
    }

    public function EditSample(){
           
        $InputData = $this->input->post();
        $InputData['indate'] = $InputData['indate'];
        $InputData['intime'] = strtotime($InputData['intime']);
        $InputData['birthdate'] = $InputData['birthdate'];

        $EditData = array(
            'SAMPLE_HN' => str_escape($InputData['hn']),
            'SAMPLE_DATE' => "(TO_DATE('".$InputData['indate']."','dd/mm/yyyy'))",
            'SAMPLE_TIME' => intval($InputData['intime']),
            'SAMPLE_PREFIX' => str_escape($InputData['prefix']),
            'SAMPLE_FNAME' => str_escape($InputData['firstname']),
            'SAMPLE_LNAME' => str_escape($InputData['lastname']),
            'SAMPLE_HBD' => "(TO_DATE('".$InputData['birthdate']."','dd/mm/yyyy'))",
            'SAMPLE_AGE' => intval($InputData['age']),
            'SAMPLE_GENDER' => str_escape($InputData['gender']),
            'SAMPLE_TYPE' => str_escape($InputData['sample_type']),
            'SAMPLE_PJOWNER' => str_escape($InputData['pjowner']),
            'SAMPLE_CONSENT' => str_escape($InputData['consent']),
            'SAMPLE_PJNAME' => str_escape($InputData['pjname']),
            'SAMPLE_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            'SAMPLE_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
            'SAMPLE_STATUS' => 0,
        );

        switch($InputData['method']){
            case 'add':
                $TargetTB = SCMPREFIX.'TR_SAMPLE';
                $EditData['SAMPLE_SID'] = str_escape($InputData['scanln']);
                $EditData['SAMPLE_CUSER'] = ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0);
                $EditData['SAMPLE_CWHEN'] = "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))";
                $this->MDL_Service_Master->Insert($TargetTB,$EditData);
                $Message = array(
                    'status' => (bool)true,
                    'message' => "Add Sample",
                    'function' => "refresh",
                );
            break;
            case 'edit':
                $TargetTB = SCMPREFIX.'TR_SAMPLE';
                $TargetID = array(
                    'SAMPLE_SID' => $InputData['scanln'],
                );
                $query_result = $this->MDL_Service_Master->Update($TargetTB,$EditData,$TargetID);
                $Message = array(
                    'status' => (bool)true,
                    'message' => "Edit Sample",
                    'function' => "refresh",
                );
            break;
        }
        echo json_encode($Message);
    }

    public function DeleteSample(){
         
        $InputData = $this->input->post();
        $TargetTB = SCMPREFIX.'TR_SAMPLE';
        $TargetID = array(
            'SAMPLE_SID' => $InputData['sample_sid'],
        );
        $this->MDL_Service_Master->Delete($TargetTB,$TargetID);
        $Message = array(
            'status' => (bool)true,
            'message' => "Delete Sample",
            'function' => "refresh",
        );
        echo json_encode($Message);    
    }

    public function EditBox(){
        $this->load->model('MDL_Biobank_Input');
        $InputData = $this->input->post();
        
        $TargetTB = SCMPREFIX.'MS_BOX';        
        $EditData = array(
            'BOX_DATE' => "(TO_DATE('".$InputData['created_date']."','dd/mm/yyyy'))",
            'BOX_TIME' => intval($InputData['created_time']),
            'BOX_NAME' => $InputData['box_name'],
            'BOX_ROW' => intval($InputData['box_row']),
            'BOX_COLUM' => intval($InputData['box_column']),
            'BOX_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            'BOX_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
            'BOX_STATUS' => 0,
        );
        switch($InputData['method']){
            case 'add':
                $EditData['BOX_BARCODE'] = str_escape($InputData['box_barcode']);
                $EditData['BOX_CUSER'] = ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0);
                $EditData['BOX_CWHEN'] = "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))";
                if( !$this->MDL_Biobank_Input->checkBox(str_escape($InputData['box_barcode'])) ){
                    $this->MDL_Service_Master->Insert($TargetTB,$EditData);
                    $Message = array(
                        'status' => (bool)true,
                        'message' => "Add Box",
                        'function' => "refresh",
                    );
                }else{
                    $Message = array(
                        'status' => (bool)false,
                        'message' => "Duplicate Box",
                        'function' => "alert",
                        'modal-data' => array(
                            'title' => 'Failed To Add New Box',
                            'text' => 'Duplicate Box Barcode',
                            'type' => 'error'
                        ),
                    );
                }
            break;
            case 'edit':
                $TargetID = array(
                    'BOX_BARCODE' => $InputData['box_barcode'],
                );
                $query_result = $this->MDL_Service_Master->Update($TargetTB,$EditData,$TargetID);
                $Message = array(
                    'status' => (bool)true,
                    'message' => "Edit Box",
                    'function' => "refresh",
                );
            break;
        }
        echo json_encode($Message);    
    }

    public function DeleteBox(){
         
        $InputData = $this->input->post();
        
        $TargetTB = SCMPREFIX.'MS_BOX';
        $TargetID = array(
            'BOX_BARCODE' => $InputData['box_barcode'],
        );
        $this->MDL_Service_Master->Delete($TargetTB,$TargetID);
        $Message = array(
            'status' => (bool)true,
            'message' => "Delete Box",
            'function' => "refresh",
        );
        echo json_encode($InputData);    
    }

    public function MasterDel(){
        $InputData = $this->input->post();
        $InputID = $InputData['del_id'];
        $InputTarget = $InputData['del_target'];
        $TargetTB = '';
        switch ($InputTarget) {
            case 'freezer':
                $TargetTB = SCMPREFIX.'MS_FREEZER';
                $TargetID = array(
                    'FREEZER_ID' => $InputID,
                );
            break;
            case 'freezerfloor':
                $TargetTB = SCMPREFIX.'MS_FREEZER_FLOOR';
                $TargetID = array(
                    'FREEZER_FLOOR_ID' => $InputID,
                );
            break;
            case 'rack':
                $TargetTB = SCMPREFIX.'MS_RACK';
                $TargetID = array(
                    'RACK_ID' => $InputID,
                );
            break;
            case 'rackfloor':
                $TargetTB = SCMPREFIX.'MS_RACK_FLOOR';
                $TargetID = array(
                    'RACK_FLOOR_ID' => $InputID,
                );
            break;
            case 'box':
                $TargetTB = SCMPREFIX.'TR_FREEZER_ADD';
                $TargetID = array(
                    'BOX_BARCODE' => $InputID,
                );
            break;
            case 'boxadd_well':
                $TargetTB = SCMPREFIX.'TR_BOX_ADD';
                $TargetID = array(
                    'BOX_ID' => $InputData['del_id'],
                    'BOXADD_WELL' => $InputData['del_boxwell'],
                );        
                $this->MDL_Service_Master->Delete($TargetTB,$TargetID);

            break;
        }
        $this->MDL_Service_Master->Delete($TargetTB,$TargetID);
        $Message = array(
            'status' => (bool)true,
            'message' => "Success",
            'function' => "refresh",
        );
        echo json_encode($Message);
    }

    public function MasterEdit(){
        $InputData = $this->input->post();
        $InputTarget = $InputData['add_target'];
        $Method = $InputData['method'];
        if($InputTarget == 'boxadd_well'){
            $InputData['tube_barcode'] = ( strlen($InputData['tube_barcode'])>10 ? substr($InputData['tube_barcode'],0,10) : $InputData['tube_barcode']  ); //ตัด 2 ตัวท้ายออก (เลข specimen)
            $Data = array(
                'SAMPLE_SID' => $InputData['tube_barcode'],
                'BOX_WELL' => $InputData['boxwell_input'],
                'BOX_ID' => $InputData['box_id'],
                'target' => 'boxadd_well',
            );
            if( $this->MasterAddProcess($Data) ){
                $Message = array(
                    'status' => (bool)true,
                    'message' => "Success",
                    'function' => "refresh",
                );
            }else{                
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Failed",
                    'function' => "alert",
                    'modal-data' => array(
                        'title' => 'Failed',
                        'text' => 'Failed To Input',
                        'type' => 'error'
                    ),
                );
                /*
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Failed",
                );
                */
            }
        }else if($InputTarget == 'box'){
            $Data = array(
                        'parent_freezer' => $InputData['parent_freezer'],
                        'parent_freezerfloor' => $InputData['parent_freezerfloor'],
                        'parent_rack' => $InputData['parent_rack'],
                        'parent_rackfloor' => $InputData['parent_rackfloor'],
                        'box_barcode' => $InputData['box_barcode'],
                        'target' => $InputTarget,
                    );
            if( $this->MasterAddProcess($Data) ){
                $Message = array(
                    'status' => (bool)true,
                    'message' => "Success",
                    'function' => "refresh",
                );
            }else{
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Failed",
                    'function' => "alert",
                    'modal-data' => array(
                        'title' => 'Failed',
                        'text' => 'Failed To Input Box',
                        'type' => 'error'
                    ),
                );
                /*
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Failed",
                );
                */
            }            
        }else{
            $InputID = $InputData['id_target'];
            $InputName = $InputData['input_name'];
            switch ($Method) {
                case 'add':
                    $Data = array(
                        'parentid' => $InputID,
                        'name' => $InputName,
                        'target' => $InputTarget,
                    );
                    if( $this->MasterAddProcess($Data) ){
                        $Message = array(
                            'status' => (bool)true,
                            'message' => "Success",
                            'function' => "refresh",
                        );                        
                    }else{
                        $Message = array(
                            'status' => (bool)false,
                            'message' => "Failed",
                            'function' => "alert",
                            'modal-data' => array(
                                'title' => 'Failed',
                                'text' => 'Failed To Add',
                                'type' => 'error'
                            ),
                        );
                        /*
                        $Message = array(
                            'status' => (bool)false,
                            'message' => "Failed",
                        );
                        */
                    }
                    break;
                case 'edit':
                    $Data = array(
                        'targetid' => $InputID,
                        'name' => $InputName,
                        'target' => $InputTarget,
                    );
                    if( $this->MasterEditProcess($Data) ){
                        $Message = array(
                            'status' => (bool)true,
                            'message' => "Success",
                            'function' => "refresh",
                        );                        
                    }else{
                        $Message = array(
                            'status' => (bool)false,
                            'message' => "Failed",
                            'function' => "alert",
                            'modal-data' => array(
                                'title' => 'Failed',
                                'text' => 'Failed To Update',
                                'type' => 'error'
                            ),
                        );
                        /*
                        $Message = array(
                            'status' => (bool)false,
                            'message' => "Failed",
                        );
                        */
                    }
                    break;
            }
        }
        echo json_encode($Message);
    }

    function MasterAddProcess($Data){
        $this->load->model('MDL_Biobank_Input');
        $TargetTB = '';
        switch ($Data['target']) {
            case 'freezer':
                $TargetTB = SCMPREFIX.'MS_FREEZER';
                $AddData = array(
                    'FREEZER_NAME' => str_escape($Data['name']),
                    'FREEZER_STATUS' => 0,
                    'FREEZER_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                    'FREEZER_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'freezerfloor':
                $TargetTB = SCMPREFIX.'MS_FREEZER_FLOOR';
                $AddData = array(
                    'FREEZER_ID' => $Data['parentid'],
                    'FREEZER_FLOOR_NAME' => str_escape($Data['name']),
                    'FREEZER_FLOOR_EDIT' => str_escape($Data['name']),
                    'FREEZER_FLOOR_STATUS' => 0,
                    'FREEZER_FLOOR_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_FLOOR_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                    'FREEZER_FLOOR_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_FLOOR_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'rack':
                $TargetTB = SCMPREFIX.'MS_RACK';
                $AddData = array(
                    'FREEZER_ID' => 0,
                    'FREEZER_FLOOR_ID' => $Data['parentid'],
                    'RACK_NAME' => str_escape($Data['name']),
                    'RACK_EDIT' => str_escape($Data['name']),
                    'RACK_STATUS' => 0,
                    'RACK_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                    'RACK_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'rackfloor':
                $TargetTB = SCMPREFIX.'MS_RACK_FLOOR';
                $AddData = array(
                    'FREEZER_ID' => 0,
                    'FREEZER_FLOOR_ID' => 0,
                    'RACK_ID' => $Data['parentid'],
                    'RACK_FLOOR_NAME' => str_escape($Data['name']),
                    'RACK_FLOOR_EDIT' => str_escape($Data['name']),
                    'RACK_FLOOR_STATUS' => 0,
                    'RACK_FLOOR_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_FLOOR_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                    'RACK_FLOOR_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_FLOOR_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'box':
                if( $this->MDL_Biobank_Input->checkBox(str_escape($Data['box_barcode'])) ){

                    if( !$this->MDL_Biobank_Input->checkAdd(str_escape($Data['box_barcode'])) ){
                        $TargetTB = SCMPREFIX.'TR_FREEZER_ADD';
                        $AddData = array(
                            'FREEZER_ID' => $Data['parent_freezer'],
                            'FREEZER_FLOOR_ID' => $Data['parent_freezerfloor'],
                            'RACK_ID' => $Data['parent_rack'],
                            'RACK_FLOOR_ID' => $Data['parent_rackfloor'],
                            'BOX_BARCODE' => str_escape($Data['box_barcode']),
                            'FREEZERADD_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                            'FREEZERADD_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                            'FREEZERADD_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                            'FREEZERADD_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                            'FREEZERADD_STATUS' => 0,
                        );
                    }
                    else{
                        $TargetTB = SCMPREFIX.'TR_FREEZER_ADD';
                        $TargetID = array(
                            'BOX_BARCODE' => $Data['box_barcode'],
                        );
                        $EditData = array(
                            'FREEZER_ID' => intval($Data['parent_freezer']),
                            'FREEZER_FLOOR_ID' => intval($Data['parent_freezerfloor']),
                            'RACK_ID' => intval($Data['parent_rack']),
                            'RACK_FLOOR_ID' => intval($Data['parent_rackfloor']),
                            'FREEZERADD_MUSER' => intval(($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0)),
                            'FREEZERADD_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                            'FREEZERADD_STATUS' => 0,
                        );

                        $query_result = $this->MDL_Service_Master->Update($TargetTB,$EditData,$TargetID);
                        return true;
                        die();
                    }
                }else{
                    //Box doesn't exists
                    return false;
                    die();
                }
            break;
            case 'boxadd_well':
                $TargetTB = SCMPREFIX.'TR_BOX_ADD';
                $AddData = array(
                    'BOX_ID' => intval($Data['BOX_ID']),
                    'BOXADD_WELL' => str_escape($Data['BOX_WELL']),
                    'SAMPLE_SID' => str_escape($Data['SAMPLE_SID']),
                    'BOXADD_STATUS' => 0,
                    'BOXADD_CUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'BOXADD_CWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                    'BOXADD_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'BOXADD_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );

                $TargetID = array(
                    'SAMPLE_SID' => $Data['SAMPLE_SID'],
                    'BOXADD_STATUS' => 0,
                );        
                $this->MDL_Service_Master->Delete($TargetTB,$TargetID);

            break;
        }
        $this->MDL_Service_Master->Insert($TargetTB,$AddData);
        return true;
    }

    function MasterEditProcess($Data){
        $TargetTB = '';
        switch ($Data['target']) {
            case 'freezer':
                $TargetTB = SCMPREFIX.'MS_FREEZER';
                $TargetID = array(
                    'FREEZER_ID' => $Data['targetid'],
                );
                $EditData = array(
                    'FREEZER_NAME' => str_escape($Data['name']),
                    'FREEZER_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'freezerfloor':
                $TargetTB = SCMPREFIX.'MS_FREEZER_FLOOR';
                $TargetID = array(
                    'FREEZER_FLOOR_ID' => $Data['targetid'],
                );
                $EditData = array(
                    'FREEZER_FLOOR_EDIT' => str_escape($Data['name']),
                    'FREEZER_FLOOR_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'FREEZER_FLOOR_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'rack':
                $TargetTB = SCMPREFIX.'MS_RACK';
                $TargetID = array(
                    'RACK_ID' => $Data['targetid'],
                );
                $EditData = array(
                    'RACK_EDIT' => str_escape($Data['name']),
                    'RACK_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
            case 'rackfloor':
                $TargetTB = SCMPREFIX.'MS_RACK_FLOOR';
                $TargetID = array(
                    'RACK_FLOOR_ID' => $Data['targetid'],
                );
                $EditData = array(
                    'RACK_FLOOR_EDIT' => str_escape($Data['name']),
                    'RACK_FLOOR_MUSER' => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    'RACK_FLOOR_MWHEN' => "(TO_DATE('".convert_ce_be(date("d/m/Y"))."','dd/mm/yyyy'))",
                );
            break;
        }        
        $this->MDL_Service_Master->Update($TargetTB,$EditData,$TargetID);
        return true;
    }

}
