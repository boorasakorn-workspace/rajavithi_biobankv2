<div class="modal fade" id="input_edit-modal" tabindex="-1" role="dialog" aria-labelledby="input_edit-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Add Box</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterEdit', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                        $target_input = array(
                            'type' => 'hidden',
                            'name' => 'add_target',
                            'id' => 'add_target',
                        );
                        $loop_hidden = array(
                            'freezer' => array(
                                'name' => 'parent_freezer',
                                'id' => 'parent_freezer',
                            ),
                            'freezerfloor' => array(
                                'name' => 'parent_freezerfloor',
                                'id' => 'parent_freezerfloor',
                            ),
                            'rack' => array(
                                'name' => 'parent_rack',
                                'id' => 'parent_rack',
                            ),
                            'rackfloor' => array(
                                'name' => 'parent_rackfloor',
                                'id' => 'parent_rackfloor',
                            ),
                        );
                        foreach($loop_hidden as $loop_result){
                            $hide = array(
                                'type' => 'hidden',
                                'name' => $loop_result['name'],
                                'id' => $loop_result['id'],
                            );
                            echo form_input($hide);
                        }
                    ?>
                    <?=form_input($method_input);?>
                    <?=form_input($target_input);?>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('box_barcode','','Box Barcode');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="input_edit_submit" data-dismiss="modal" form_submit>Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="input_del-modal" tabindex="-1" role="dialog" aria-labelledby="input_del-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterDel', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'del_id',
                            'id' => 'del_id',
                        );
                        $target_input = array(
                            'type' => 'hidden',
                            'name' => 'del_target',
                            'id' => 'del_target',
                        );
                    ?>
                    <?=form_input($hidden_input);?>
                    <?=form_input($target_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_target"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>