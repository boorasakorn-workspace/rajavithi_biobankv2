<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('click','[modal-add]',function(){
			$('#input_edit-modal .modal-title').text('Add');
			$('#input_edit-modal input#method').val('add');
			var tAttr = $(this).attr('modal-add');
			var appendTitle = '';
			var add_target = '';
			switch(tAttr) {
			case 'box':
				appendTitle = 'Box';
				add_target = 'box';
				$('#input_edit-modal input#parent_freezer').val( $(this).data('freezerid') );
				$('#input_edit-modal input#parent_freezerfloor').val( $(this).data('freezerfloorid') );
				$('#input_edit-modal input#parent_rack').val( $(this).data('rackid') );
				$('#input_edit-modal input#parent_rackfloor').val( $(this).data('rackfloorid') );
				break;
			}
			$('#input_edit-modal input#add_target').val(add_target);
			$('#input_edit-modal .modal-title').text('Add'+' '+appendTitle);
			$('#input_edit-modal #input_edit_submit').text('Add'+' '+appendTitle);
			$('#input_edit-modal').modal('show');
		});
		$('#input_edit-modal').on('shown.bs.modal', function () {
		    $('#box_barcode').focus();
		})  

		$(document).on('click','[modal-del]',function(){
			$('#input_edit-modal .modal-title').text('Delete');
			var tID = $(this).data('id');
			var tAttr = $(this).attr('modal-del');
			var appendTitle = '';
			var del_target = '';
			switch(tAttr) {
			case 'box':
				appendTitle = 'Box';
				del_target = 'box';
				break;
			}
			$('#input_del-modal input#del_id').val(tID);
			$('#input_del-modal input#del_target').val(del_target);
			$('#input_del-modal .modal-title').text('Delete'+' '+appendTitle);
			$('#input_del-modal').modal('show');
		});

	})
</script>