<script type="text/javascript">
	$(document).ready(function(){

		$('#boxadd_active [data-boxwell_active]').each(function(index){
			var BoxAddActive = $(this).data('boxwell_active');
			$('[data-boxwell="'+BoxAddActive+'"]').addClass('active');
			$('[data-boxwell="'+BoxAddActive+'"] button').append('<i class="fas fa-vial"></i>');
			$('[data-boxwell="'+BoxAddActive+'"] button').attr("disabled", true);;
		});

		$(document).on('click','[data-boxwell_slot]',function(){
			var BoxID = $(this).data('boxid');
			var TargetWell = $(this).data('boxwell_slot');
			$('#tube_barcode').val('');
			$('#box_input-modal input#box_id').val(BoxID);
			$('#box_input-modal input#boxwell_input').val(TargetWell);
			$('#box_input-modal').modal('show');
		});
		$('#box_input-modal').on('shown.bs.modal', function () {
		    $('#tube_barcode').focus();
		})  

		$(document).on('click','[data-boxwell_remove]',function(){
			var BoxID = $(this).data('boxid');
			var BoxWell = $(this).data('boxwell_remove');
			$('#box_del-modal span#span_target').text(BoxWell);
			$('#box_del-modal input#del_id').val(BoxID);
			$('#box_del-modal input#del_boxwell').val(BoxWell);
			$('#box_del-modal').modal('show');
		});

	});
</script>