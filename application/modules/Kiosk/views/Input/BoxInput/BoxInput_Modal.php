<div class="modal fade" id="box_input-modal" tabindex="-1" role="dialog" aria-labelledby="box_input-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Add Tube</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterEdit', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $hidden_method = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                        $hidden_target = array(
                            'type' => 'hidden',
                            'name' => 'add_target',
                            'id' => 'add_target',
                        );
                        $target_input = array(
                            'type' => 'hidden',
                            'name' => 'box_id',
                            'id' => 'box_id',
                        );
                        $well_input = array(
                            'type' => 'hidden',
                            'name' => 'boxwell_input',
                            'id' => 'boxwell_input',
                        );
                    ?>
                    <?=form_input($hidden_method,'add');?>
                    <?=form_input($hidden_target,'boxadd_well');?>
                    <?=form_input($target_input);?>
                    <?=form_input($well_input);?>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('tube_barcode','','Tube Barcode');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="box_input_submit" data-dismiss="modal" form_submit>Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="box_del-modal" tabindex="-1" role="dialog" aria-labelledby="box_del-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/MasterDel', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_target = array(
                            'type' => 'hidden',
                            'name' => 'del_target',
                            'id' => 'del_target',
                        );
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'del_id',
                            'id' => 'del_id',
                        );
                        $hidden_id_input = array(
                            'type' => 'hidden',
                            'name' => 'del_boxwell',
                            'id' => 'del_boxwell',
                        );
                    ?>
                    <?=form_input($hidden_target,'boxadd_well');?>
                    <?=form_input($hidden_input);?>
                    <?=form_input($hidden_id_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_target"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>