<script type="text/javascript">
	function removeDuplicateRows($table,callback){
	    function getVisibleRowText($row){
	        return $row.find('td:visible').not('td:first').text().toLowerCase();
	    }

	    $table.find('tr').each(function(index, row){
	        var $row = $(row);
	        $row.nextAll('tr').each(function(index, next){
	            var $next = $(next);
	            if(getVisibleRowText($next) == getVisibleRowText($row))
	                $next.remove();
	        })
	    });
	    if(callback){
	    	callback;
	    }
	}

	$(document).ready(function(){
		console.log("Ready");
		removeDuplicateRows($('#SampleTable'));
		var UserTable = $('#SampleTable').DataTable({
				ordering: false,
				scrollY: true,
				scrollX: true,
				paging: true,
				pageLength: 10,
				dom: 'frtp',
			});

		$('#management_content').on('resize', function () {
			UserTable.draw();
		});
		
		$(document).on('click','[tube-location]',function(){
			var SampleID = $(this).attr('tube-location');
			$.get( "<?=base_url('Service/TubeLocate/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length){
						$('#'+key).html(data['result'][key]);					
					}					
				});

				let btn_loop = {
					'box':{
						'id':'barcode_box',
						'button':'#box_button',
						'href':'<?=base_url('Kiosk/Input/BoxInput/');?>',	
					},
				};

				Object.keys(btn_loop).forEach(function (key) {
					if(data['result'][btn_loop[key]['id']] != null && data['result'][btn_loop[key]['id']] != ''){
						var href_target = btn_loop[key]['href'];
						var append_target = data['result'][btn_loop[key]['id']];
						$(btn_loop[key]['button']).attr('href',href_target+append_target);
						$(btn_loop[key]['button']+' button').attr('disabled',false);
					}else{
						$(btn_loop[key]['button']+' button').attr('disabled',true);
					}				
				});

				$('#tube_locate-modal').modal('show');
			});			
		});
		
		$(document).on('click','[info-sample]',function(){
			var SampleID = $(this).attr('info-sample');
			$.get( "<?=base_url('Kiosk/GetSample/');?>"+SampleID, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#info_'+key).length ){
						$('#info_'+key).val(data['result'][key]);					
					}					
				});
				$('#info_sample-modal').modal('show');
			});
		});

	})
</script>