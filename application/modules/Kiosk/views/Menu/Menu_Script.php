<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click','[output-tube]',function(){
			$('#output-modal .modal-title').text('Output Tube');
			$('#output-modal input#input_barcode').val('');
			$('#output-modal input#method').val('tube');
		
			$('#output-modal').modal('show');
		});
		$(document).on('click','[output-box]',function(){
			$('#output-modal .modal-title').text('Output Box');
			$('#output-modal input#input_barcode').val('');
			$('#output-modal input#method').val('box');

			$('#output-modal').modal('show');
		});
		$('#output-modal').on('shown.bs.modal', function () {
		    $('#input_barcode').focus();
		})  
	})
</script>