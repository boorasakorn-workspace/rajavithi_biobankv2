<div class="modal fade" id="output-modal" tabindex="-1" role="dialog" aria-labelledby="output-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h2 id="modal-title" class="modal-title">Output</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Service/Output', 'formOutput');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                    ?>
                    <?=form_input($method_input);?>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('input_barcode','','Scan Barcode');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="font-size:2rem;" id="output_submit" data-dismiss="modal" form_submit>Confirm</button>
                    <button type="button" class="btn btn-secondary" style="font-size:2rem;" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>