<div class="card rounded-0 my-auto" id="login-form">
    <div class="card-body">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <?=assets_img('images/icon_header.png','style="margin: 0 auto;"');?>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row text-decoration-none text-capitalize">
                    <div class="col-sm-12 col-lg-6" style="margin:2rem 0";>
                        <a href="<?=base_url('Kiosk/Search/');?>">
                            <button type="button" class="btn btn-success mb-1 m-t-10 btn-block" style="font-size:4rem;">Search</button>
                        </a>
                    </div>
                    <div class="col-sm-12 col-lg-6" style="margin:2rem 0";>
                        <a href="<?=base_url('Kiosk/Input/');?>">
                            <button type="button" class="btn btn-success mb-1 m-t-10 btn-block" style="font-size:4rem;">Input</button>
                        </a>
                    </div>
                    <div class="col-sm-12 col-lg-6" style="margin:2rem 0";>
                        <button type="button" class="btn btn-danger mb-1 m-t-10 btn-block" output-tube="" style="font-size:4rem;">Output Tube</button>
                    </div>
                    <div class="col-sm-12 col-lg-6" style="margin:2rem 0";>
                        <button type="button" class="btn btn-danger mb-1 m-t-10 btn-block" output-box="" style="font-size:4rem;">Output Box</button>
                    </div>
                </div>            
            </div>
        </div>
    </div>
</div>