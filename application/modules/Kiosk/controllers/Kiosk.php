<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kiosk extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');

        if(!$this->sessionextend->HasID()){
            redirect('Authentication/Kiosk','refresh');
        }

        if( NULL !== $this->sessionextend->GetSite() && $this->sessionextend->GetSite() != 'Kiosk'){
            redirect($this->sessionextend->GetSite(),'refresh');
        }

        if(!$this->sessionextend->GetFreezer() ){
            redirect('Setup','refresh');
        }
        $this->load->module('Service');
    }

    public function Index(){
        redirect('Kiosk/Menu','refresh');
    }

    public function Logout(){
        foreach($_SESSION as $key => $val){
            if ($key !== 'FreezerID' && $key !== 'site'){
              unset($_SESSION[$key]);
            }
        }
        redirect('','refresh');
    }
    
    public function Menu(){
        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Kiosk',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Menu/Menu',
                'Modal' => 'Menu/Menu_Modal',
            ),
            'Script' => array(
                'MainScript' => 'Menu/Menu_Script',
            ),
        );
        $this->template_module->Template('TabletTemplate',$Template);;      
    }

    public function Search(){
        $this->load->module('Template_Module');
        $Template = array(
            'Module' => 'Kiosk',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Search/Search',
                'Modal' => 'Search/Search_Modal',
            ),
            'Script' => array(
                'MainScript' => 'Search/Search_Script',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.js',
            ),
            'Data' => array(
                'Sample' => $this->service->FullTubeLocate($this->sessionextend->GetFreezer()),
            ),
        );

        $this->template_module->Template('TabletTemplate',$Template);; 
    }

    public function Input($SelectedInput = NULL,$ID = NULL){
        $this->load->module('Template_Module');
        switch ($SelectedInput) {
            case 'All':
                $InputTitle = 'Freezer';
                $InputData = array(
                    'FreezerData'   =>  $this->sessionextend->GetFreezer(),
                    'FreezerFloorData'  =>  $this->service->ListAdd('FreezerFloor'),
                    'RackData'  =>  $this->service->ListAdd('Rack'),
                    'RackFloorData' =>  $this->service->ListAdd('RackFloor'),
                    'FreezerBoxData' => $this->service->ListAdd('Box'),
                );
                $InputContent = array(
                    'Main' => 'Input/Input',
                    'Modal' => 'Input/Input_Modal',
                );
                $ScriptContent = array(
                    'MainScript' => 'Input/Input_Script',
                );
                break;
            case 'BoxInput':
                $InputTitle = 'Box';
                $BoxData = $this->service->ListData('Box',$ID);
                $InputData = array(
                    'BoxID' => $BoxData->BOX_ID,
                    'BoxInput' => $this->service->DataAdd('Box',$ID),
                    'Slot' => array(
                        'Rows' => $BoxData->BOX_ROW,
                        'Columns' => $BoxData->BOX_COLUM,
                    ),
                );
                $InputContent = array(
                    'Main' => 'Input/BoxInput/BoxInput',
                    'Modal' => 'Input/BoxInput/BoxInput_Modal',
                );
                $ScriptContent = array(
                    'MainScript' => 'Input/BoxInput/BoxInput_Script',
                );
                break;    
            default:
                redirect('Kiosk/Input/All','refresh');
                break;
        }

        $Template = array(
            'Module' => 'Kiosk',
            'Site_Title' => (isset($InputTitle) && $InputTitle != '' ? $InputTitle : 'Input'),
            'Content' => (isset($InputContent) && count($InputContent) > 0 ? $InputContent : array()),
            'Script' => (isset($ScriptContent) && count($ScriptContent) > 0 ? $ScriptContent : array()),
            'Data' => array(
                'InputData' => $InputData,
            )
        );
        $this->template_module->Template('TabletTemplate',$Template);
    }

}