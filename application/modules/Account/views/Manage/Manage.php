<div class="card rounded-0" id="register-form">
    <div class="card-header">
        <h3 class="mb-0">Manage Account</h3>
    </div>
    <div class="card-body">
        <?=form_multipart_extend('Account/Edit', 'formRegister');?>
            <div class="row">
                <div class="col">
                    <?=form_group_input('staffid',$Data['AccountData']['staffid'],'StaffID');?>
                </div>	
            </div>
            <div class="row">
                <div class="col-1">
                    <?php
                        echo form_label(ucfirst('prefix'), 'prefix');
                        echo dropdown_prefix($Data['AccountData']['prefix']); 
                    ?>
                </div>
                <div class="col">
                    <?=form_group_input('firstname',$Data['AccountData']['firstname']);?>
                </div>
                <div class="col">
                    <?=form_group_input('lastname',$Data['AccountData']['lastname']);?>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <?php
                            echo form_label(ucfirst('gender'), 'gender');
                            echo dropdown_gender($Data['AccountData']['gender']);
                        ?>
                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group">
                        <?php
                            echo form_label(ucfirst('birthdate'), 'Birthdate');
                            echo datepicker_th('birthdate',$Data['AccountData']['birthdate']);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_input('email',$Data['AccountData']['email'],'E-mail');?>
                </div>
                <div class="col">
                    <?=form_group_input('phone',$Data['AccountData']['phone']);?>
                </div>
            </div>
            <button type="button" class="btn btn-success btn-lg float-right" form_submit>Save Change</button>
        <?=form_close();?>
    </div>
</div>
<div class="card rounded-0" id="register-form">
    <div class="card-header">
        <h3 class="mb-0">Change Password</h3>
    </div>
    <div class="card-body">
        <?=form_multipart_extend('Account/ChangePassword', 'formPassword');?>
            <div class="row">
                <div class="col">
                    <?=form_group_password('password_old','','Old Password');?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_password('password_new','','New Password');?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_password('password_new_repeat','','Repeat New Password');?>
                </div>
            </div>
            <button type="button" class="btn btn-success btn-lg float-right" form_submit>Change Password</button>
        <?=form_close();?>
    </div>
</div>