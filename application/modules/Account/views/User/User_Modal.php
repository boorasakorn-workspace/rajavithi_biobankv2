<div class="modal fade" id="edit_user-modal" tabindex="-1" role="dialog" aria-labelledby="edit_user-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-title" class="modal-title">Edit User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Account/EditUser', 'formEdit');?>
                <div class="modal-body">                    
                    <?php 
                        $method_input = array(
                            'type' => 'hidden',
                            'name' => 'method',
                            'id' => 'method',
                        );
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'userid',
                            'id' => 'edit_userid',
                        );
                    ?>
                    <?=form_input($method_input);?>
                    <?=form_input($hidden_input);?>
                    <div id="add_input" class="row">
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('staffid','','StaffID');?>
                        </div>  
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('position'), 'position');
                                    echo dropdown_usertype('');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <?php
                                echo form_label(ucfirst('prefix'), 'prefix');
                                echo dropdown_prefix(''); 
                            ?>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('gender'), 'gender');
                                    echo dropdown_gender('');
                                ?>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <?php
                                    echo form_label(ucfirst('birthdate'), 'Birthdate');
                                    echo datepicker_th('birthdate','');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('firstname','');?>
                        </div>
                        <div class="col">
                            <?=form_group_input('lastname','');?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?=form_group_input('email','','E-mail');?>
                        </div>
                        <div class="col">
                            <?=form_group_input('phone','');?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="user_edit_submit" data-dismiss="modal" form_submit>Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>

<div class="modal fade" id="del_user-modal" tabindex="-1" role="dialog" aria-labelledby="del_user-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=form_multipart_extend('Account/DeleteUser', 'formDelete');?>
                <div class="modal-body">
                    <?php 
                        $hidden_input = array(
                            'type' => 'hidden',
                            'name' => 'userid',
                            'id' => 'del_userid',
                        );
                    ?>
                    <?=form_input($hidden_input);?>
                    <div class="row">
                        <div class="col">
                            <h5>ต้องการจะลบ <span id="span_fullname"></span> ใช่หรือไม่</h5>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal" form_submit>Delete</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </div>
            <?=form_close();?>
        </div>
    </div>
</div>