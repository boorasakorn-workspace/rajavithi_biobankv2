<div class="card rounded-0" id="register-form">
    <div class="card-header">
		<h3>User</h3>
	</div>
</div>
<div class="card-body">
	<div class="row">
		<div class="col"><h4>User</h4></div>
		<div class="col text-right">
			<button type="button" class="btn btn-success mb-1" add-user> New User </button>
		</div>
	</div>
	<div class="container-fluid">
		<table id="UserTable" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
				    <th style="white-space: nowrap;">#</th>
				    <th style="white-space: nowrap;">Name</th>  
                    <th style="white-space: nowrap;">Gender</th>
                    <th style="white-space: nowrap;">Birthday</th>
				    <th style="white-space: nowrap;">Age</th>
				    <th style="white-space: nowrap;">Email</th>
                    <th style="white-space: nowrap;">Phone</th>
                    <th style="white-space: nowrap;">Position</th>
				    <th style="white-space: nowrap;">Manage</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($Data['User'] as $key => $User): ?>
					<tr>
					    <td style="white-space: nowrap;"><?=$key;?></td>
					    <td style="white-space: nowrap;"><?=$User->USER_FULLNAME?></td>  
	                    <td style="white-space: nowrap;"><?=$User->USER_GENDER;?></td>
	                    <td style="white-space: nowrap;"><?=convert_ce_be(convert_date_format($User->USER_HBD));?></td>
					    <td style="white-space: nowrap;"><?=$User->USER_AGE;?></td>
					    <td style="white-space: nowrap;"><?=$User->USER_EMAIL;?></td>
	                    <td style="white-space: nowrap;"><?=$User->USER_PHONE;?></td>
	                    <td style="white-space: nowrap;"><?=$User->USER_TYPE_NAME;?></td>
					    <td style="white-space: nowrap;">
							<button type="button" class="btn btn-sm btn-warning" edit-user="<?=$User->USER_ID;?>">Edit</button>
							<button type="button" class="btn btn-sm btn-danger" del-user="<?=$User->USER_ID;?>">Delete</button>
					    </td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>