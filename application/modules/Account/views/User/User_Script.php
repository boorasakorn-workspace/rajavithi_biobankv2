<script type="text/javascript">
	$(document).ready(function(){

		var UserTable = $('#UserTable').DataTable({
				ordering: false,
				scrollY: true,
				scrollX: true,
				paging: false,
				dom: 'rt',
			});

		$('#management_content').on('resize', function () {
			UserTable.draw();
		});

		$(document).on('click','[add-user]',function(){
			$('#method').val('add');
			var add_input = '';
			add_input +='<div class="col">';
            add_input +='<div class="form-group">';
			add_input +='<label for="username">Username</label>';
			add_input +='<input type="text" name="username" value="" id="username" class="form-control form-control-lg rounded-0" placeholder="">';
			add_input +='</div>';
            add_input +='</div>';
            add_input +='<div class="col">';
            add_input +='<div class="form-group">';
			add_input +='<label for="password">Password</label>';
			add_input +='<input type="password" name="password" value="" id="password" class="form-control form-control-lg rounded-0" placeholder="">';
			add_input +='</div>';
            add_input +='</div>';
			$('#add_input').html(add_input);
			$('#user_edit_submit').text('Add');
			$('#modal-title').text('Add User');
			$('#edit_user-modal').modal('show');
		});

		$(document).on('click','[edit-user]',function(){
			var UserID = $(this).attr('edit-user');
			$.get( "Account/GetUserID/"+UserID, function( data ) {
				data = JSON.parse(data);
				console.log(data);
				Object.keys(data['result']).forEach(function (key) {
					if( $('#'+key).length ){
						$('#'+key).val(data['result'][key]);					
					}					
				});
				$('#add_input').html('');
				$('#method').val('edit');
				$('#edit_userid').val(data['result']['userid']);
				$('#user_edit_submit').text('Edit');
				$('#modal-title').text('Edit User');
				$('#edit_user-modal').modal('show');
			});
		});

		$(document).on('click','[del-user]',function(){
			var UserID = $(this).attr('del-user');
			$.get( "Account/GetUserID/"+UserID, function( data ) {
				data = JSON.parse(data);
				var targetFullname = data['result']['prefix'] + ' ' + data['result']['firstname'] + ' ' + data['result']['lastname'];
				$('#span_fullname').text(targetFullname);
				$('#del_userid').val(data['result']['userid']);
				$('#del_user-modal').modal('show');
			});
		});

	});
</script>