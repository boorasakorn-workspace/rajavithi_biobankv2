<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');

        if(!$this->sessionextend->HasID()){
            redirect('Authentication','refresh');
        }
        
        if( NULL !== $this->sessionextend->GetSite() && $this->sessionextend->GetSite() != 'Management'){
            redirect($this->sessionextend->GetSite(),'refresh');
        }
        
        $this->load->model('MDL_Account');
    }

    public function Index(){
        redirect('Account/Manage','refresh');
    }

    public function Manage($ID = NULL){
        $ID = ($ID != NULL ? $ID : $this->sessionextend->currentID());
        if( ($ID != $this->sessionextend->currentID()) && ($this->session->userdata('type') != 0) ){
            redirect('Account/Manage','refresh');
        }

        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Account',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Manage/Manage',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
            ),
            'Data' => array(
                'AccountData' => $this->MDL_Account->GetUserID($ID),
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template);;               
    }

    public function User(){
        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Account',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'User/User',
                'Modal' => 'User/User_Modal',
            ),
            'Script' => array(
                'Main' => 'User/User_Script',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
                'jqueryDataTable' => 'vendors/jquerydataTable/datatables.min.js',
            ),
            'Data' => array(
                'User' => $this->MDL_Account->GetUser(array("USER_STATUS !=" => '2',"USER_TYPE !=" => '0')),
            ),
        );
        $this->template_module->Template('ManagementTemplate',$Template);;               
    }

    public function Edit(){
        $InputData = $this->input->post();
        $Data = array(
            "USER_ID" => (isset($InputData['userid']) && $InputData['userid'] != ''? $InputData['userid'] : $this->sessionextend->currentID()),
            "USER_IDCARD" => $InputData['staffid'],
            "USER_PREFIX" => "'".$InputData['prefix']."'",
            "USER_FNAME" => "'".$InputData['firstname']."'",
            "USER_LNAME" => "'".$InputData['lastname']."'",
            "USER_HBD" => "(TO_DATE('".$InputData['birthdate']."','dd/mm/yyyy'))",
            "USER_AGE" => calc_age($InputData['birthdate']),
            "USER_GENDER" => "'".$InputData['gender']."'",
            "USER_EMAIL" => "'".$InputData['email']."'",
            "USER_PHONE" => "'".$InputData['phone']."'",
            "USER_MUSER" => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            "USER_MWHEN" => "(TO_DATE('".date("d/m/Y")."','dd/mm/yyyy'))",
        );
        $this->MDL_Account->UpdateUser($Data,$Data['USER_ID']);
        $Message = array(
            'status' => (bool)true,
            'message' => "User Update Success",
            'function' => "refresh",
        );
        echo json_encode($Message);
    }

    public function ChangePassword(){
        $InputData = $this->input->post();
        if($InputData['password_new'] == $InputData['password_new_repeat']){
            $OldPassword = md5($InputData['password_old']);
            $CheckData = array(
                'USER_ID' => $this->sessionextend->currentID(),
                'USER_PASSWORD' => $OldPassword,
            );
            $PasswordCheck = $this->MDL_Account->GetUserInfo($CheckData);
            if(isset($PasswordCheck) && $PasswordCheck != NULL){
                if($InputData['password_old'] != $InputData['password_new']){
                    $NewPassword = md5($InputData['password_new']);
                    $UserID = $this->sessionextend->currentID();
                    $UserData = array(
                        'USER_PASSWORD' => $NewPassword,
                    );
                    $this->MDL_Account->UpdateUser($UserData,$UserID);
                    $Message = array(
                        'status' => (bool)true,
                        'message' => "Change Password Success",
                        'function' => "refresh",
                    );
                }else{
                    $Message = array(
                        'status' => (bool)false,
                        'message' => "Old Password and New Password is the same",
                    );
                }
            }else{
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Wrong Old Password",
                );
            }           
        }else{
            $Message = array(
                'status' => (bool)false,
                'message' => "New Password doesn't match",
            );    
        }
        echo json_encode($Message);
    }

    public function GetUserID($ID){
        if( $this->sessionextend->userType() == 0 ){
            $Query = $this->MDL_Account->GetUserID($ID);
            $Message = array(
                'status' => (bool)true,
                'message' => 'Successful',
                'result' => $Query,
            );
        }else{            
            $Message = array(
                'status' => (bool)false,
                'message' => "No Permission",
            );
        }
        echo json_encode($Message);
    }


    public function EditUser(){
        $InputData = $this->input->post();        
        $Data = array(
            "USER_IDCARD" => $InputData['staffid'],
            "USER_TYPE" => $InputData['usertype'],
            "USER_PREFIX" => "'".$InputData['prefix']."'",
            "USER_FNAME" => "'".$InputData['firstname']."'",
            "USER_LNAME" => "'".$InputData['lastname']."'",
            "USER_HBD" => "(TO_DATE('".$InputData['birthdate']."','dd/mm/yyyy'))",
            "USER_AGE" => calc_age($InputData['birthdate']),
            "USER_GENDER" => ( $InputData['gender'] ? "'".$InputData['gender']."'" : "''" ),
            "USER_EMAIL" => "'".$InputData['email']."'",
            "USER_PHONE" => "'".$InputData['phone']."'",
            "USER_STATUS" => 1,
            "USER_MUSER" => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
            "USER_MWHEN" => "(TO_DATE('".date("d/m/Y")."','dd/mm/yyyy'))",
        );
        switch ($InputData['method']) {
            case 'add':
                $Data = array_merge($Data,array(
                    "USER_USERNAME" => "'".$InputData['username']."'",
                    "USER_PASSWORD" => "'".md5($InputData['password'])."'",
                    "USER_IMAGE" => "'blank'",
                    "USER_IMAGENAME" => "'blank'",
                    "USER_CUSER" => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                    "USER_CWHEN" => "(TO_DATE('".date("d/m/Y")."','dd/mm/yyyy'))",
                ));
                $this->MDL_Account->InsertUser($Data);
                $Message = array(
                    'status' => (bool)true,
                    'message' => "User Add Success",
                    'user' => $this->sessionextend->currentID(),
                    'function' => 'refresh',
                );
                break;
            case 'edit':
                $UserID = $InputData['userid'];
                $this->MDL_Account->UpdateUser($Data,$UserID);
                $Message = array(
                    'status' => (bool)true,
                    'message' => "User Update Success",
                    'user' => $this->sessionextend->currentID(),
                    'function' => 'refresh',
                );
                break;            
            default:
                $Message = array(
                    'status' => (bool)false,
                    'message' => "Undefined Method",
                );
                break;
        }               
        echo json_encode($Message);
    }

    public function DeleteUser(){
        if( $this->sessionextend->userType() == 0 ){
            $InputData = $this->input->post();
            $Data = array(
                "USER_ID" => $InputData['userid'],
            );
            $this->MDL_Account->DeleteUser($Data);
            $Message = array(
                'status' => (bool)true,
                'message' => "Delete " . $Data['USER_ID'] . " Successful",
                'function' => 'refresh',
            );
        }else{
            $Message = array(
                'status' => (bool)false,
                'message' => "No Permission",
            );
        }
        echo json_encode($Message);
    }

}