<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_Account extends CI_Model {
    function __construct(){
        parent::__construct();
    }

    function GetUser($Data){
        $case_usertype = "        
            CASE USER_TYPE
               WHEN 0 THEN 'ADMIN'
               WHEN 1 THEN 'STAFF'
               WHEN 2 THEN 'HEAD STAFF'
               WHEN 3 THEN 'USER'
               ELSE 'USER'
            END
        AS USER_TYPE_NAME ";
        $query_fullname = " USER_ANU.USER_PREFIX || ' ' || USER_ANU.USER_FNAME || ' ' ||  USER_ANU.USER_LNAME AS USER_FULLNAME ";
        $this->db->select('USER_ANU.*');
        $this->db->select($case_usertype);
        $this->db->select($query_fullname);
        $this->db->from(SCMPREFIX.'USER_ANU');
        $this->db->where($Data);
        $query = $this->db->get();
        return $query->result();
    }

    function GetUserID($ID){
        $case_usertype = "        
            CASE USER_TYPE
               WHEN 0 THEN 'ADMIN'
               WHEN 1 THEN 'STAFF'
               WHEN 2 THEN 'HEAD STAFF'
               WHEN 3 THEN 'USER'
               ELSE 'USER'
            END
        AS USER_TYPE_NAME ";
        $query_fullname = " USER_ANU.USER_PREFIX || ' ' || USER_ANU.USER_FNAME || ' ' ||  USER_ANU.USER_LNAME AS USER_FULLNAME ";
        $this->db->select('USER_ANU.*');
        $this->db->select($case_usertype);
        $this->db->select($query_fullname);
        $this->db->from(SCMPREFIX.'USER_ANU');
        $this->db->where('USER_ID',$ID);
        $query = $this->db->get();
        $Data = array(
            'userid' => $query->row()->USER_ID,
            'staffid' => $query->row()->USER_IDCARD,
            'username' => $query->row()->USER_USERNAME,
            'password' => $query->row()->USER_PASSWORD,
            'prefix' => $query->row()->USER_PREFIX,
            'firstname' => $query->row()->USER_FNAME,
            'lastname' => $query->row()->USER_LNAME,
            'fullname' => $query->row()->USER_FULLNAME,
            'birthdate' => convert_date_format($query->row()->USER_HBD),
            'age' => $query->row()->USER_AGE,
            'gender' => $query->row()->USER_GENDER,
            'email' => $query->row()->USER_EMAIL,
            'phone' => $query->row()->USER_PHONE,
            'usertype' => $query->row()->USER_TYPE,
            'usertype_name' => $query->row()->USER_TYPE_NAME,
            'userstatus' => $query->row()->USER_STATUS,
            'userimage' => $query->row()->USER_IMAGE,
            'userimagename' => $query->row()->USER_IMAGENAME,
            'cuser' => $query->row()->USER_CUSER,
            'cwhen' => convert_date_format($query->row()->USER_CWHEN),
            'muser' => $query->row()->USER_MUSER,
            'mwhen' => convert_date_format($query->row()->USER_MWHEN),
        );
        return $Data;
    }

	function GetUserInfo($Data){      
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'USER_ANU');
        $this->db->where($Data);
        $query = $this->db->get();
        return $query->row();
    }

    function InsertUser($Data){
        $this->db->insert(SCMPREFIX.'USER_ANU',$Data,false);
    }

    function UpdateUser($Data,$ID){
        foreach($Data as $key => $value){
           $this->db->set($key,$value,FALSE); 
       }        
        $this->db->where('"USER_ID"',$ID,false);
        $this->db->update(SCMPREFIX.'USER_ANU');
    }

    function DeleteUser($Data){
        $this->db->delete(SCMPREFIX.'USER_ANU', $Data);
    }
}