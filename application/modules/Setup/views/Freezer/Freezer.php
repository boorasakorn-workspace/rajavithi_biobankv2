<div class="card">
    <div class="card-header">
        <div class="row">
	        <div class="col">
	            <?=(isset($Site_Title) && $Site_Title != '' ? $Site_Title : 'Freezer Setup');?>
	            <a href="<?=base_url('Kiosk/Menu');?>">
	                <button class="btn btn-success m-b-40 m-t-10">
	                    Back
	                </button>
	            </a>
	        </div>
	    </div>
    </div>
    <div class="card-body">
    <!-- Freezer Body -->
        <div class="row">
            <?php 
            if(isset($Data['Freezer']) && $Data['Freezer']->num_rows() > 0){
                foreach ($Data['Freezer']->result() as $Row) : 
            ?>
                <div class="col-4 text-center" style="margin: 0 auto;max-width:100%;">
                    <a href="<?=base_url('Setup/FreezerSetup/').$Row->FREEZER_ID;?>">
                        <?=assets_img('images/icon/management/freezer.png',array('width'=>'70%','alt'=>$Row->FREEZER_NAME));?>
                        <h3 class="text-center p-t-10 p-b-20"><?=$Row->FREEZER_NAME;?></h3>
                    </a>
                </div>

            <?php 
                endforeach;
            }
            ?>
        </div>
    <!-- Freezer Body -->
    </div>
</div>