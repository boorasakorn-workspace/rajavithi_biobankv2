<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');

        if( NULL !== $this->sessionextend->GetSite() && $this->sessionextend->GetSite() != 'Kiosk'){
            redirect($this->sessionextend->GetSite(),'refresh');
        }
    }

    public function Index(){
        redirect('Setup/Freezer','refresh');
    }

    public function Freezer(){
        $this->load->module('Template_Module');
        $this->load->model('MDL_List');
        $Template = array(
            'Module' => 'Setup',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Freezer/Freezer',
            ),
            'Data' => array(
                'Freezer' => $this->MDL_List->Freezer(),
            ),
        );
        $this->template_module->Template('AuthenticationTemplate',$Template);; 
    }

    public function FreezerSetup($FreezerID){
        $this->sessionextend->SetSite('Kiosk');
        $this->sessionextend->SetFreezer($FreezerID);
        redirect('Kiosk','refresh');
    }
}