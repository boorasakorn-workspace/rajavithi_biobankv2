<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_List extends CI_Model {
    function __construct(){
        parent::__construct();
    }
  	
    // Freezer
    public function Freezer(){
        $this->db->select('*');
        $this->db->from(SCMPREFIX.'MS_FREEZER');
        $this->db->where('FREEZER_STATUS !=','1',false);
        $this->db->order_by('FREEZER_NAME','ASC');
        $query = $this->db->get();      
        return $query;
    }

}