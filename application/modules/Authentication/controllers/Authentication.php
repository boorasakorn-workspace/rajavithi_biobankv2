<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('assets');
        $this->load->helper('form');
        $this->load->helper('form_extend');

        if($this->sessionextend->HasID() && $this->sessionextend->GetSite()){
            redirect($this->sessionextend->GetSite(),'refresh');
        }
    }

    public function Index(){
        $Site = ($this->sessionextend->GetSite()? $this->sessionextend->GetSite() : 'Kiosk');
        redirect('Authentication/'.$Site,'refresh');
    }

    public function Management(){
        $this->Login('Management');
    }

    public function Kiosk(){
        $this->Login('Kiosk');
    }

    public function Login($Site = NULL){
        $this->load->module('Template_Module');

        switch ($Site) {
            case 'Management':
                $MainContent = 'Login/Login_Management';
                break;
            case 'Kiosk':
                $MainContent = 'Login/Login_Tablet';
                break;            
            default:
                $MainContent = 'Login/Login_Management';
                break;
        }

        $Template = array(
            'Module' => 'Authentication',
            'Site_Title' => '',
            'Content' => array(
                'Main' => $MainContent,
            ),
            'Script' => array(
                'MainScript' => 'Login/Login_Script',
            )
        );
        $this->template_module->Template('AuthenticationTemplate',$Template);
    }

    public function Register(){
        $this->load->module('Template_Module');

        $Template = array(
            'Module' => 'Authentication',
            'Site_Title' => '',
            'Content' => array(
                'Main' => 'Register/Register',
            ),
            'css' => array(                
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/css/datepicker.css', 
            ),
            'js' => array(
                'bootstrap-datepicker' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
                'bootstrap-datepicker-extension' => 'vendors/bootstrap-datepicker/js/bootstrap-datepicker-thai.js',
                'bootstrap-datepicker-th' => 'vendors/bootstrap-datepicker/js/locales/bootstrap-datepicker.th.js',
            ),
        );
        $this->template_module->Template('AuthenticationTemplate',$Template);
    }

    public function LoginProcess(){
        $this->load->model('MDL_Authentication');        
        $InputData = $this->input->post();
        if( isset($InputData['username']) && isset($InputData['password']) ){
            $Data = array(
                "USER_USERNAME" => $InputData['username'],
                "USER_PASSWORD" => md5($InputData['password']),
            );
            $AuthenSession = 'Management';
        }else if( isset($InputData['scan_idcard']) ){
            $Data = array(
                "USER_IDCARD" => $InputData['scan_idcard'],
            );
            $AuthenSession = 'Kiosk';          
        }
        $query = $this->MDL_Authentication->GetUser($Data);
        if(isset($query) && $query != NULL){
            $sessionData = array(
                'id' => $query->USER_ID,
                'user_username' => $query->USER_USERNAME,
                'user_name' => $query->USER_FULLNAME,
                'type' => $query->USER_TYPE,
                'site' => $AuthenSession,
            );
            $this->sessionextend->SetSession($sessionData);
            $Message = array(
                'status' => (bool)true,
                'message' => 'Login Success',
                'function' => 'redirect',
                'url' => $AuthenSession,
            );
        }else{
            $Message = array(
                'status' => (bool)false,
                'message' => "Login Failed",
                'function' => "alert",
                'modal-data' => array(
                    'title' => 'Failed',
                    'text' => 'Login Failed',
                    'type' => 'error'
                ),
            );
            /*
            $Message = array(
                'status' => (bool)false,
                'message' => 'Login Failed',
            );
            */
        }
        echo json_encode($Message);
    }

    public function RegisterProcess(){
        $this->load->model('MDL_Authentication');        
        $InputData = $this->input->post();
        $CheckData = array(
            "USER_USERNAME" => $InputData['username'],
        );
        $query = $this->MDL_Authentication->GetUser($CheckData);
        if(isset($query) && $query != NULL){
            $Message = array(
                'status' => (bool)false,
                'message' => "This Username already Exists",
                'function' => "alert",
                'modal-data' => array(
                    'title' => 'Duplicate Username',
                    'text' => 'This Username already Exists',
                    'type' => 'error'
                ),
            );
            /*
            $Message = array(
                'status' => (bool)false,
                'message' => 'This Username already Exists',
            );
            */
        }else{
            $Data = array(
                "USER_IDCARD" => $InputData['staffid'],
                "USER_USERNAME" => "'".$InputData['username']."'",
                "USER_PASSWORD" => "'".md5($InputData['password'])."'",
                "USER_PREFIX" => "'".$InputData['prefix']."'",
                "USER_FNAME" => "'".$InputData['firstname']."'",
                "USER_LNAME" => "'".$InputData['lastname']."'",
                "USER_HBD" => "(TO_DATE('".$InputData['birthdate']."','dd/mm/yyyy'))",
                "USER_AGE" => calc_age($InputData['birthdate']),
                "USER_GENDER" => "'".$InputData['gender']."'",
                "USER_EMAIL" => "'".$InputData['email']."'",
                "USER_PHONE" => "'".$InputData['phone']."'",
                "USER_TYPE" => 3,
                "USER_STATUS" => 1,
                "USER_IMAGE" => "'blank'",
                "USER_IMAGENAME" => "'blank'",
                "USER_CUSER" => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                "USER_CWHEN" => "(TO_DATE('".date("d/m/Y")."','dd/mm/yyyy'))",
                "USER_MUSER" => ($this->sessionextend->HasID() ? $this->sessionextend->currentID() : 0),
                "USER_MWHEN" => "(TO_DATE('".date("d/m/Y")."','dd/mm/yyyy'))",
            );
            $query = $this->MDL_Authentication->InsertUser($Data);
            $Message = array(
                'status' => (bool)true,
                'message' => 'Register Success',
                'function' => 'redirect',
                'url' => 'Authentication/Login',
            );
        }
        echo json_encode($Message);
    }

}
