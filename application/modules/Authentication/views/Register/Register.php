<div class="card rounded-0" id="register-form">
    <div class="card-header">
        <h3 class="mb-0">New Account</h3>
    </div>
    <div class="card-body">
        <?=form_multipart_extend('Authentication/RegisterProcess', 'formRegister');?>
            <div class="row">
                <div class="col">
                    <?=form_group_input('username');?>
                </div>
                <div class="col">
                    <?=form_group_password('password');?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_input('staffid','','StaffID');?>
                </div>
                <div class="col">
                    <?php
                        echo form_label(ucfirst('prefix'), 'prefix');
                        echo dropdown_prefix(''); 
                    ?>
                </div>
                <div class="col">
                    <?=form_group_input('firstname');?>
                </div>
                <div class="col">
                    <?=form_group_input('lastname');?>
                </div>
            </div>
            <div class="row">
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="form-group">
                        <?php
                            echo form_label(ucfirst('gender'), 'gender');
                            echo dropdown_gender();
                        ?>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <?php
                            echo form_label(ucfirst('birthdate'), 'Birthdate');
                            echo datepicker_th('birthdate');
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_input('email','','E-mail');?>
                </div>
                <div class="col">
                    <?=form_group_input('phone');?>
                </div>
            </div>
            <div>
                <label class="custom-control custom-checkbox">
                 I have an account. <a href="<?=base_url('Authentication/Login');?>" class="login-form-link">Login.</a>
                </label>
            </div>
            <button type="button" class="btn btn-success btn-lg float-right" form_submit>Register</button>
        <?=form_close();?>
    </div>
</div>