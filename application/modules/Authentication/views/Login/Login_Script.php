<script type="text/javascript">
	$(document).ready(function(){
		if( $('#scan_idcard').length ){
			$('#scan_idcard').focus();
			// Force focus
			$('#scan_idcard').focusout(function () {
				$('#scan_idcard').focus();
			});
			$('#scan_idcard').keyup(function(){
		        if(this.value.length !=''){
		            $('[form_submit]').click();
		        }
		    });
		}			
	});
</script>