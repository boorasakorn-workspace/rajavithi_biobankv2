<div class="card rounded-0" id="login-form">
    <div class="card-header">
        <h3 class="mb-0">User Login</h3>
    </div>
    <div class="card-body">
    	<?=form_multipart_extend('Authentication/LoginProcess', 'formLogin');?>
            <div class="row">
                <div class="col">
                    <?=form_group_input('username');?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?=form_group_password('password');?>
                </div>
            </div>
            <label class="custom-control custom-checkbox">
                I don't have an account. <a href="<?=base_url('Authentication/Register');?>" class="login-form-link">Register</a>
            </label>
            <button type="button" class="btn btn-success btn-lg float-right" form_submit>Login</button>
        <?=form_close();?>
    </div>
</div>