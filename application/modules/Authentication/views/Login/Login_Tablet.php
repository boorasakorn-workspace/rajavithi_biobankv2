<div class="card rounded-0" id="login-form">
    <div class="card-body">
    	<?=form_multipart_extend('Authentication/LoginProcess', 'formLogin');?>
            <div class="row">
                <div class="col text-center">
                    <?=assets_img('images/icon_header.png','style="margin: 0 auto;"');?>
                </div>
            </div>
            <div class="row">
                <div class="col" style="font-size:3rem;">
                    <?=form_group_input('scan_idcard','','Scan IDCard');?>
                    <button type="button" class="btn btn-success btn-block" style="font-size:3rem;" form_submit>Login</button>
                </div>
            </div>            
        <?=form_close();?>
    </div>
</div>