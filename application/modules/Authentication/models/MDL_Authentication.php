<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDL_Authentication extends CI_Model {
    function __construct(){
        parent::__construct();
    }

	function GetUser($Data){
        $case_usertype = "        
            CASE USER_TYPE
               WHEN 0 THEN 'ADMIN'
               WHEN 1 THEN 'STAFF'
               WHEN 2 THEN 'HEAD STAFF'
               WHEN 3 THEN 'USER'
               ELSE 'USER'
            END
        AS USER_TYPE_NAME ";
        $query_fullname = " USER_ANU.USER_PREFIX || ' ' || USER_ANU.USER_FNAME || ' ' ||  USER_ANU.USER_LNAME AS USER_FULLNAME ";
        $this->db->select('USER_ANU.*',FALSE);
        $this->db->select($case_usertype,FALSE);
        $this->db->select($query_fullname,FALSE);
        $this->db->from(SCMPREFIX.'USER_ANU');
        $this->db->where($Data);
        $query = $this->db->get();
        return $query->row();
    }

    function InsertUser($Data){
        $this->db->insert(SCMPREFIX.'USER_ANU',$Data,false);
    }
}