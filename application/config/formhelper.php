<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['form_class'] = 'form';
$config['input_class'] = 'form-control form-control-lg rounded-0';