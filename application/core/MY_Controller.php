<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
	// Set Current URL for security website
	private function currentUrl(){
		if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443){
			$protocol = 'https://';
		}else{
			$protocol = 'http://';
		}

		return $protocol;
	}
	
}